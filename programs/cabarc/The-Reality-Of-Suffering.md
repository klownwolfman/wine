## The Reality Of Suffering

 
 
 
 
**The Reality Of Suffering 🆓 [https://shoxet.com/2tHw3M](https://shoxet.com/2tHw3M)**

 
 
 
 
 
It is specifically in the heart of darkness, the lived reality of everyday traumas, that we may enlarge our faith, making room not only for light but for darkness as well. By confronting the reality of suffering in all its existential realness, we learn that faith is born not in spite of suffering, but through it.
 
Discomfort is reality. Pain is reality. Conflict is reality. Spiritual warfare is reality. Healthy faith helps us embrace all of these biblical realities, constantly availing ourselves of the reality of Christ's help and presence. Unhealthy, toxic faith denies the dark side, thus creating an even greater conflict.
 
A healthy faith accepts who we are and where we are rather than trying to conjure some artificial image for people who are not comfortable accepting us as we are. If we're based in reality, then the reality is that we've all failed, we're all sinners, and we're all stumbling along the way. We thrash about in slimy pits now and then; we feel fear as we walk narrow trails in dark valleys.
 
Do you see what John was saying about living in reality? If we choose the path of deception and deny the obvious truth, in so doing we call God a liar! If, on the other hand, we walk in reality, that is, "if we walk in the light, as he is in the light, we have fellowship with one another, and the blood of Jesus, his Son, purifies us from all sin" (verse 7). That's where I want to be. Walking in the light. Admitting who I am. Enjoying the companionship of Christian friends and the Lord Jesus himself, experiencing daily cleansing from sin.
 
Healthy faith refuses to sweep suffering, daily struggles with the sinful nature, and inevitable relationship difficulties under a rug, pretending they don't exist. Instead, it brings those issues into the light of Scripture, the scrutiny of the Holy Spirit, and under the mutual counsel and care of trusted brothers and sisters in Christ. Unhealthy faith, on the other hand, denies reality. For those who subscribe to such false belief systems, faith is not based on a belief in the supernatural power of God, but on a desire to see magical solutions that stop pain. They hope in a servant God determined to make life easy.
 
Because faith grows strong, there is no need to deny reality. Believing God is faithful to help them through their trails and tribulations, healthy believers have no need to run from reality. They see the problems before them, do what they can to resolve them, and trust God to do the rest.
 
Without understanding the source of suffering, human beings strive to gain happiness by possessiveness and greed, through violence and hatred. We act out of delusion and ignorance, creating pain as an inevitable result. Our grasping, our aggressive entanglement in the world brings with it unavoidable struggle and loss, yet all is done purportedly to seek safety, to find happiness. The Buddha saw what every wise heart comes to know, that life on earth is painful as well as beautiful. Yet our confused reactions magnify this fundamental pain into an even greater suffering. Millions hunger, though there is abundant food available. Millions languish in sickrooms or hospitals, ill with diseases that we have medicine to cure. This suffering is not separate from us.
 
There are times in spiritual life when it feels as if all the barriers we have erected to shield ourselves from the pains of the world have crumbled. Our hearts become tender and raw and we feel a natural kinship with all that lives. The cries of street children echo in our mind, images of terrorism and racism, ecological destruction, poverty, and slavery fill our consciousness. It is as if our consciousness has broken open to the struggles of humanity and the earth itself. We may feel that we are in a charnel ground, we may see the suffering of countless generations. And we recognize that there is no escape from this. Yet only by opening our eyes and heart to the suffering of the world can we find freedom or peace. In our own way, each of us must look into this great question: What is the truth about suffering in human life and what is the cause of this suffering?
 
To see the truth of suffering completely is to come to freedom through the gate of suffering. We can never successfully grasp or control the changing conditions of life. We cannot possess our lover, our spouse, our homes, our work. We cannot even possess our children. Yes, we can love and care for them, but if we try to control them, we only create suffering. Pleasure and pain, praise and blame, success and failure alternate day after day. The world itself has pain and pleasure woven into it as night is woven together with day. If we resist this truth, we will inevitably suffer. When we open the heart through the gate of sorrow, we sense how pain and dissatisfaction are woven into the fabric of experience.
 
One strategy for gaining liberation is to focus our attention directly upon this inherent, continuous experience of dissatisfaction and pain. We must sense it clearly, and find in its midst a freedom that releases us from any identification or grasping. By the careful observation of the source of each action, a constant movement to alleviate suffering is revealed. Yet those who face this truth do not find it a formula for despair, but a gateway to compassion. For within the heart is found a freedom and love even greater than the suffering. By facing the pain of the world they awaken a fearless and merciful heart, the universal birthright of humanity.
 
We create self-inflicted pain. The more we share our suffering with others, the more we suffer. We suffer when we are lonely and then suffer from our relationships. We suffer over not having money and then suffer over being rich or not being wealthy enough. The dramatization of our suffering creates a snowball effect.
 
We must embrace rather than overcome our suffering. In Yoga and meditation, pain is to be allowed, felt, and embraced. Avoidance leads to further suffering. Be in the moment with the pain. What is it trying to tell you? Do not hold it longer, not let it go too soon. Be with it.
 
A very pertinent post. Our species history is littered with the again and again of conflict. We seem to be able to forget the mistakes from the past without learning the intense lesson of suffering that indeed is always the same. And runs the risk of always being the same again, both now at stretching out into the future. I think we ignore the terrible pain of our violent conflicts at our peril.
 
In age-appropriate ways, my husband and I are seeking to lay a foundation for our children that includes the reality of hardship and suffering. We pray they will see God in those times and learn to trust him through them. Here are a few suggestions of some discussions that we are having in our home.
 
Our Western culture has an odd relationship to death. We try to escape the reality of death and aging, celebrating youth, and constantly chasing after it. We sanitize our field of vision from human misery and suffering, yet we are bombarded with images of violence and death on our screens. Our fear of suffering and death is evidenced in our attempts to control death through the legalization of assisted suicide.
 
In many societies and communities around the world, suffering and death is sadly the norm. Latin America theologian Gustavo Gutierrez, OP, draws our attention to the plight of so many millions who live in situations of death every day. For Gutierrez, the poverty he lives means death, lack of food, housing, education, healthcare, respect, dignity, freedom. Exploitation of the vulnerable and systemic violence is the never-ending lot of so many. He asks: how can we proclaim a God of life in this situation of death?
 
There are miraculous healings and deliverances, but this is not the norm either naturally or supernaturally. The fact is: God has not removed suffering from this world though He has given us the means to have the eternal consequences of sin removed.
 
The Book of Job actually speaks to a pretty common experience that people can have. If bad things happen to you, a lot of people feel an enormous pressure when they talk to you to find some consolation that will actively remove the pain and the loss and the suffering you feel. Sometimes those attempts are much more about making the consoler feel comfortable in the situation.
 
The Buddha is often compared to a physician. In the first two Noble Truths he diagnosed the problem (suffering) and identified its cause. The third Noble Truth is the realisation that there is a cure.
 
Some people who encounter this teaching may find it pessimistic. Buddhists find it neither optimistic nor pessimistic, but realistic. Fortunately the Buddha's teachings do not end with suffering; rather, they go on to tell us what we can do about it and how to end it.
 
Our day-to-day troubles may seem to have easily identifiable causes: thirst, pain from an injury, sadness from the loss of a loved one. In the second of his Noble Truths, though, the Buddha claimed to have found the cause of all suffering - and it is much more deeply rooted than our immediate worries.
 
The Buddha discouraged his followers from asking too many questions about nirvana. He wanted them to concentrate on the task at hand, which was freeing themselves from the cycle of suffering. Asking questions is like quibbling with the doctor who is trying to save your life.
 
In *Pain is a Reality, Suffering is a Choice*(Mosaica Press), my friend Rabbi Asher Resnick takes that question head-on and provides the reader with a framework and answers on dealing with one of the most intractable issues a person could ever face. Resnick writes not just from his head but also from his heart. The book is the outcome of his dealin