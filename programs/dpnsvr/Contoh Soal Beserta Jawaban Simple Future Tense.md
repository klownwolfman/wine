## Contoh Soal Beserta Jawaban Simple Future Tense

 
 ![Contoh Soal Beserta Jawaban Simple Future Tense](https://1.bp.blogspot.com/-ylXEijTnCyw/Ur4XCL69pXI/AAAAAAAAB4A/Ti6Fg2OmAas/w0/SFT+kurus.gif)
 
 
**## Downloadable file links:
[Link 1](https://imgfil.com/2tAfBB)

[Link 2](https://blltly.com/2tAfBB)

[Link 3](https://bltlly.com/2tAfBB)

**

 
 
 
 
 
# Contoh Soal Beserta Jawaban Simple Future Tense
 
Simple future tense adalah bentuk kalimat yang digunakan untuk menyatakan suatu kejadian yang akan terjadi di masa depan. Ada beberapa cara untuk membentuk simple future tense, yaitu dengan menggunakan modal will, be going to, atau present continuous tense. Berikut adalah contoh soal beserta jawaban simple future tense yang bisa Anda pelajari.
 
## Contoh Soal dan Jawaban Simple Future Tense dengan Will
 
Will adalah modal yang digunakan untuk mengekspresikan keputusan spontan, prediksi, janji, atau harapan di masa depan. Rumusnya adalah will + verb (bare infinitive). Berikut adalah contoh soal dan jawaban simple future tense dengan will:
 
1. A: What are you going to do this weekend? (Apa yang akan kamu lakukan akhir pekan ini?)

B: I don't know. I think I **will stay** at home and watch Netflix. (Aku tidak tahu. Aku rasa aku akan tinggal di rumah dan menonton Netflix.)
2. A: The weather forecast says it's going to rain tomorrow. (Prakiraan cuaca mengatakan akan hujan besok.)

B: Really? Then I **will bring** my umbrella. (Benarkah? Kalau begitu aku akan membawa payungku.)
3. A: I'm sorry I forgot your birthday. (Maaf aku lupa ulang tahunmu.)

B: It's okay. I'm sure you **will remember** it next year. (Tidak apa-apa. Aku yakin kamu akan mengingatnya tahun depan.)
4. A: Do you think he will pass the exam? (Menurutmu dia akan lulus ujian?)

B: Yes, I do. He **will study** hard for it. (Ya, aku yakin. Dia akan belajar keras untuk itu.)
5. A: I'm really hungry. (Aku sangat lapar.)

B: Don't worry. I **will make** you some sandwiches. (Jangan khawatir. Aku akan membuatkanmu beberapa roti lapis.)

## Contoh Soal dan Jawaban Simple Future Tense dengan Be Going To
 
Be going to adalah bentuk kalimat yang digunakan untuk menyatakan suatu rencana, niat, atau prediksi berdasarkan bukti di masa depan. Rumusnya adalah be + going to + verb (bare infinitive). Berikut adalah contoh soal dan jawaban simple future tense dengan be going to:

1. A: What are you going to do this weekend? (Apa yang akan kamu lakukan akhir pekan ini?)

B: I **am going to visit** my grandparents in Bandung. (Aku akan mengunjungi kakek nenekku di Bandung.)
2. A: Look at those dark clouds. (Lihatlah awan-awan gelap itu.)

B: It **is going to rain** soon. We should go home now. (Akan segera hujan. Kita harus pulang sekarang.)
3. A: I have a toothache. (Aku sakit gigi.)

B: You **are going to see** a dentist tomorrow, right? (Kamu akan pergi ke dokter gigi besok, kan?)
4. A: She has been studying hard for the TOEFL test. (Dia telah belajar keras untuk tes TOEFL.)

B: She **is going to get** a high score for sure. (Dia pasti akan mendapatkan skor tinggi.)
5. A: He looks very tired and sleepy. (Dia terlihat sangat

## Contoh Soal dan Jawaban Simple Future Tense dengan Present Continuous Tense

    Present continuous tense juga bisa digunakan untuk menyatakan suatu kejadian yang akan terjadi di masa depan, terutama jika sudah ada perencanaan atau persiapan sebelumnya. Rumusnya adalah be + verb (present participle). Berikut adalah contoh soal dan jawaban simple future tense dengan present continuous tense:

    1. A: What are you doing this weekend? (Apa yang sedang kamu lakukan akhir pekan ini?)

B: I **am flying** to Bali with my family. We have booked the tickets and the hotel. (Aku sedang terbang ke Bali bersama keluargaku. Kami sudah memesan tiket dan hotel.)
    2. A: Are you ready for the presentation tomorrow? (Apakah kamu siap untuk presentasi besok?)

B: Yes, I am. I **am presenting** the first part and you are presenting the second part, right? (Ya, aku siap. Aku sedang menyajikan bagian pertama dan kamu sedang menyajikan bagian kedua, kan?)
    3. A: Why are you wearing a coat and a scarf? (Mengapa kamu memakai mantel dan syal?)

B: Because I **am leaving** for Canada in an hour. It's very cold there. (Karena aku sedang berangkat ke Kanada dalam satu jam. Di sana sangat dingin.)
    4. A: She is not answering her phone. (Dia tidak menjawab teleponnya.)

B: Maybe she **is sleeping**. She has a night shift tonight. (Mungkin dia sedang tidur. Dia memiliki shift malam hari ini.)
    5. A: He is very excited today. (Dia sangat bersemangat hari ini.)

B: Of course he is. He **is graduating** from college tomorrow. (Tentu saja dia bersemangat. Dia sedang lulus dari perguruan tinggi besok.)

## Kesimpulan

    Simple future tense adalah bentuk kalimat yang digunakan untuk menyatakan suatu kejadian yang akan terjadi di masa depan. Ada beberapa cara untuk membentuk simple future tense, yaitu dengan menggunakan modal will, be going to, atau present continuous tense. Setiap cara memiliki penggunaan dan makna yang berbeda-beda. Semoga contoh soal dan jawaban simple future tense di atas bermanfaat untuk Anda.
 842aa4382a




