## Year 4 Homework Literacy

 
  
 
**## File links for downloading:
[Link 1](https://cinurl.com/2tETaX)

[Link 2](https://tlniurl.com/2tETaX)

[Link 3](https://byltly.com/2tETaX)

**

 
 
 
 
 
The activity sheets are structured around the narrative, non-fiction and poetry blocks of the new literacy Framework. The content comes from common Year 4 fiction and non-fiction themes. The activities are designed to support work done across the curriculum as well as in literacy teaching.
 
The activities follow the main literacy priorities in Year 4 and are designed to be used flexibly. They are intended to be used with an adult: it would be pointless for the child to do them alone. Much of the learning is in the interaction.
 
When homework is set on TTRS, sections will be greyed out so they can't access anything other than their homework. A headphone symbol will appear which tells them how many games have been completed out of the amount set.
 
Homework is one aspect of the general education curriculum that has been widely recognized as important to academic success. Teachers have long used homework to provide additional learning time, strengthen study and organizational skills, and in some respects, keep parents informed of their children's progress.
 
Generally, when students with disabilities participate in the general education curriculum, they are expected to complete homework along with their peers. But, just as students with disabilities may need instructional accommodations in the classroom, they may also need homework accommodations.
 
It is important to check out all accommodations with other teachers, students, and their families. If teachers, students, or families do not find homework accommodations palatable, they may not use them.
 
Teachers can enhance homework completion and accuracy by providing classroom instruction in organizational skills. They should talk with parents about how to support the application of organizational skills at home.
 
Students with disabilities often need additional organizational support. Just as adults use calendars, schedulers, lists, and other devices to self-monitor activities, students can benefit from these tools as well. Students with disabilities can monitor their own homework using a planning calendar to keep track of homework assignments. Homework planners also can double as home-school communication tools if they include a space next to each assignment for messages from teachers and parents.
 
Students developed their own homework calendars. Each page in the calendar reflected one week. There was a space for students to write their homework assignments and a column for parent-teacher notes. The cover was a heavy card stock that children decorated. Students were expected to take their homework planners home each day and return them the next day to class.
 
Homework accounts for one-fifth of the time that successful students invest in academic tasks, yet students complete homework in environments over which teachers have no control. Given the fact that many students experience learning difficulties, this creates a major dilemma. Teachers and parents of students with disabilities must communicate clearly and effectively with one another about homework policies, required practices, mutual expectations, student performance on homework, homework completion difficulties, and other homework-related concerns.
 
Epstein, M., Munk, D., Bursuck, W., Polloway, E., & Jayanthi, M. (1999). Strategies for improving home-school communication about homework for students with disabilities. The Journal of Special Education, 33(3), 166-176.
 
Jayanthi, M., Sawyer, V., Nelson, J., Bursuck, W., & Epstein, M. (1995). Recommendations for homework-communication problems: From parents, classroom teachers, and special education teachers. Remedial and Special Education, 16(4), 212-225.
 
For many parents, homework is the only picture they get of what their child does at school. For this reason, Hamilton have put a great deal of effort into writing suitable, fun and home-friendly activities for children and parents to share together. Children will benefit and so will parents and teachers!
 
In year 4, your child will start to learn more complex punctuation, demonstrate good spelling and punctuation, and continue to develop their reading and writing. Our year 4 English worksheets will help support your child in all their literacy learning, including poetry activities, information texts, reading comprehensions, and much more.
 
It used to be that students were the only ones complaining about the practice of assigning homework. For years, teachers and parents thought that homework was a necessary tool when educating children. But studies about the effectiveness of homework have been conflicting and inconclusive, leading some adults to argue that homework should become a thing of the past.
 
According to Duke professor Harris Cooper, it's important that students have homework. His meta-analysis of homework studies showed a correlation between completing homework and academic success, at least in older grades. He recommends following a "10 minute rule": students should receive 10 minutes of homework per day in first grade, and 10 additional minutes each subsequent year, so that by twelfth grade they are completing 120 minutes of homework daily.
 
But his analysis didn't prove that students did better because they did homework; it simply showed a correlation. This could simply mean that kids who do homework are more committed to doing well in school. Cooper also found that some research showed that homework caused physical and emotional stress, and created negative attitudes about learning. He suggested that more research needed to be done on homework's effect on kids.
 
Some researchers say that the question isn't whether kids should have homework. It's more about what kind of homework students have and how much. To be effective, homework has to meet students' needs. For example, some middle school teachers have found success with online math homework that's adapted to each student's level of understanding. But when middle school students were assigned more than an hour and a half of homework, their math and science test scores went down.
 
Researchers at Indiana University discovered that math and science homework may improve standardized test grades, but they found no difference in course grades between students who did homework and those who didn't. These researchers theorize that homework doesn't result in more content mastery, but in greater familiarity with the kinds of questions that appear on standardized tests. According to Professor Adam Maltese, one of the study's authors, "Our results hint that maybe homework is not being used as well as it could be."
 
In an article in *Education Week Teacher*, teacher Samantha Hulsman said she's frequently heard parents complain that a 30-minute homework assignment turns into a three-hour battle with their kids. Now, she's facing the same problem with her own kids, which has her rethinking her former beliefs about homework. "I think parents expect their children to have homework nightly, and teachers assign daily homework because it's what we've always done," she explained. Today, Hulsman said, it's more important to know how to collaborate and solve problems than it is to know specific facts.
 
Child psychologist Kenneth Barish wrote in *Psychology Today* that battles over homework rarely result in a child's improvement in school. Children who don't do their homework are not lazy, he said, but they may be frustrated, discouraged, or anxious. And for kids with learning disabilities, homework is like "running with a sprained ankle. It's doable, but painful."
 
Last year, the public schools in Marion County, Florida, decided on a no-homework policy for all of their elementary students. Instead, kids read nightly for 20 minutes. Superintendent Heidi Maier said the decision was based on Cooper's research showing that elementary students gain little from homework, but a lot from reading.
 
Orchard Elementary School in South Burlington, Vermont, followed the same path, substituting reading for homework. The homework policy has four parts: read nightly, go outside and play, have dinner with your family, and get a good night's sleep. Principal Mark Trifilio says that his staff and parents support the idea.
 
But while many elementary schools are considering no-homework policies, middle schools and high schools have been reluctant to abandon homework. Schools say parents support homework and teachers know it can be helpful when it is specific and follows certain guidelines. For example, practicing solving word problems can be helpful, but there's no reason to assign 50 problems when 10 will do. Recognizing that not all kids have the time, space, and home support to do homework is important, so it shouldn't be counted as part of a student's grade.
 
Should you ban homework in your classroom? If you teach lower grades, it's possible. If you teach middle or high school, probably not. But all teachers should think carefully about their homework policies. By limiting the amount of homework and improving the quality of assignments, you can improve learning outcomes for your students.
 
While I do believe children should read purely for the enjoyment of reading, I knew as a teacher I wanted a bit more. I wanted something that I could give my students that would NOT be overwhelming and would reinforce the reading skills I wanted them to practice. This is exactly why I FINALLY created a reading homework system that would solve my problems.
 
The **4th grade reading comprehension activities** below are coordinated with the 4th grade spelling words curriculum on a week-to-week basis, so both can be used together as part of a comprehensive program, or each can be used separately. The worksheets inc