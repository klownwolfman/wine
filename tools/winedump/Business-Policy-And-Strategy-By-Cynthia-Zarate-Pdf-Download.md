## Business Policy And Strategy By Cynthia Zarate Pdf Download

 
  
 
**LINK ->>> [https://poitaihanew.blogspot.com/?l=2tHFRE](https://poitaihanew.blogspot.com/?l=2tHFRE)**

 
 
 
 
 
Leadership targeting has become a key feature of counterterrorism policy. Both academics and policymakers have argued that the removal of leaders is an effective strategy in combating terrorism. Leadership decapitation is not always successful, however, and existing empirical work does not account for this variability. A theory of organizational resilience explains why decapitation results in the decline of some terrorist organizations and the survival of others. Organizational resilience is dependent on two variables: bureaucratization and communal support. Older and larger organizations tend to develop bureaucratic features, facilitating a clear succession process and increasing their stability and ability to withstand attacks on their leadership. Communal support plays an important role in providing the resources necessary for terrorist groups to function and survive. Religious and separatist groups typically enjoy a high degree of support from the communities in which they operate, and thus access to critical resources. Application of this theoretical model to the case of al-Qaida reveals that Osama bin Laden's death and the subsequent targeting of other high-level al-Qaida operatives are unlikely to produce significant organizational decline.
 
Despite these and other instances of successful targeting, al-Qaida remains a resilient terrorist organization. Applying a theory of organizational resilience, I examine why targeting al-Qaida's leadership is not an effective counterterrorism strategy and, indeed, is likely counterproductive. A terrorist group's ability to withstand attacks is a function of two factors: bureaucratization and communal support. Analyzing both when and why certain terrorist groups are able to survive leadership attacks, this article differs from existing work by providing a more nuanced lens through which to evaluate the effectiveness of counterterrorism policy.
 
Regardless of the effectiveness and potential for adverse consequences of its decapitation strategy, the United States is likely to continue targeting al-Qaida leaders because U.S. policymakers view the killing of high-level targets, such as bin Laden, al-Libi, al-Rahman, Kashmiri, and Mauritania, as successes in themselves. Ultimately, however, leadership targeting alone is not enough to effectively fight a strong and emboldened terrorist organization.
 72ea393242
 
 
