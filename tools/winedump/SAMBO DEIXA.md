## SAMBO DEIXA

 
  
 
**## Files available for download:
[Link 1](https://cinurl.com/2tDMvN)

[Link 2](https://tlniurl.com/2tDMvN)

[Link 3](https://byltly.com/2tDMvN)

**

 
 
 
 
 
Laŭ sia moderna formo la sambo ekestis proksimume 1920 en Rio-de-Ĵanejro. Tiutempe la ĉoro-orkestroj perdis sian gravecon, aperis ĵazbando kaj salonorkestroj, kiuj muzikis fokstroton, maŝiŝojn, marŝojn kaj sambojn. Sambo ekestis el miksaĵo de ĉoro kun la *batukoj*, kiujn oni tiam ludis en la antaŭurboj de Rio-de-Ĵanejro.
 
Samba tamen ankaŭ eniris la blankulan, burĝan medion. La *samba-canção* [sambokanto] pli emfazis la melodion, havis pli malrapidan tempon kaj pli bone poluritajn tekstojn. Per la apero de la radiofonio sambo rapidege disvastiĝis kaj fariĝis en la 1930-aj la muzika pulsodonanto de la lando. En 1939 Ary Barroso komponis la faman muziktitolon *Aquarela do Brasil*, kiu en la mallonga formo *Brazil* famiĝis kaj travivis multajn interpretadojn. En la sama jaro la sambokantistino Carmen Miranda iris Usonen, kie ŝi supreniĝis kiel plej bone pagata aktoro kaj kantistino de Holivudo.
 
En la malfruaj 1950-aj jaroj pli kaj pli elementoj de bolero, fokstroto kaj ĉaĉao enpenetris en la sambon, kiu en tiu ĉi tempo perdis laŭgrade siajn tipajn distingilojn. Tiu ĉi degenero estis ekigilo por muzika revolucio nome bosanovo. Alie ol la strata sambo bosanovo ekestis en la urba mezklaso en la medio de burĝa intelektularo. Stilformanto estis ĉefe João Gilberto, kaj per sia mallaŭta kanto kaj ankaŭ per sia maniero ludi la gitaron. La sinretenema kantado turnis la operetecan belkanto-stilon, kiu estis venkinta en la sambo de la 1950-aj jaroj, en la malon. Iniciatilo por la tutmonda trarompo de bosanovo estis la filmo *Orfeu Negro*, kiu en 1959 ricevis Premion Oskaro kaj la Oran Palmon dum la Festivalo de Cannes. La filmo baziĝas sur teatraĵo de Vinícius de Moraes, kiu verkis tekstojn por sambokantoj por kvar muzikistaj generacioj. Li translokis la antikvan miton de Orfeo en la estantecon de la karnavalo de Rio-de-Ĵanejro. La filmmuzikon komponis Tom Jobim kaj Luiz Bonfá, kies titolkantoj *A Felicidade* kaj *Manhã de Carnaval* estis fariĝontaj klasikaĵoj de bosanovo. Krom tio la strata sambo de karnavalo ĉiam denove trapasas la filmon. Vinícius de Moraes kaj Tom Jobim ankaŭ verkis kune la kanton *Garota de Ipanema*, kies anglalingva versio *Girl from Ipanema* fariĝis la plej fama bosanova kanto, kiun ĝis nun en kaj ekster Brazilo interpretas multaj bosanovaj kaj ĵazaj muzikistoj. Simile grandiozan sukceson ekhavis Sérgio Mendes per sia kanto *Mas Que Nada*. Post kiam la kuba influo sur la muzikon de Usono post la revolucio de 1953 ĝis 1959 estis malpliiĝinta, nun Brazilo fariĝis per bosanovo la plej grava impulsdonanto por nordamerika latinida ĵazo. Ekde la 1990-aj jaroj bosanovo travivis renaskiĝon per novinterpretadoj ekzemple de Bebel Gilberto kaj per adaptaĵoj en la elektronika muziko.
 
La 44 sambolernejoj de Rio-de-Ĵaneiro estas bone organizitaj asocioj kun parte pluraj miloj da membroj, kiuj transprenas la preparon, la probojn, sed ankaŭ sociajn taskojn en siaj respektivaj (formalaj aŭ malformalaj) kvartaloj. Pro la altaj kostoj de la paradoj kun la elspezaj kostumoj kaj alegorie ornamitaj ĉaroj kaj pro la granda turisma intereso la karnavalo en Rio-de-Ĵaneiro estas treege komercigita, kaj ekzistas parte rilatoj kun la mafioj, kiuj uzas la sambolernejojn por monlavado.
 
La lernejoj paradas tra avenuo ĉirkaŭata de tribunoj, centoj da membroj de ĉiu lernejo sin vestas per unuformaj roboj, dancas sambomuzikon laŭ koreografioj antaŭe aranĝitaj kaj laŭ originalaj muzikaĵoj. Ĉiu lernejo elektas apartan temon kiel centra motivo, ekzemple historian eventon, faman personon aŭ brasildevenan legendon. La kanto de la samba temo devas pritrakti la elektitan temon, kaj la parado de ĉiu lernejo devas disvolvi la temon per siaj kostumoj, pentraĵoj aŭ skulptaĵo el papermaĉaĵo.
 
La plej konataj sambolernejoj estas tiuj de la metropola regiono de Rio-de-Ĵanejro, kiuj paradas en la Sambódromo de Rio-de-Ĵanejro. Ekzistas ankaŭ sambolernejoj en preskaŭ ĉiuj federacieroj brazilaj kaj en multaj landoj de la mondo.[4][5] La sambolernejoj de Rio-de-Ĵanejro oni konsideras kiel gvida, se ne la ĉefa montrofenestro de la brazila karnavalo,[6] kaj aktuale ili pli kaj pli alprenas surscenigan aspekton kun kelkaj dramaj, teatraj kaj koreografiaj ecoj.[7]
 
*Samba de roda* kaj *Samba de caboclo* apartenas al la plej originaj variaĵoj de sambo. En ĉi tiuj stiloj oni tradicie uzas la atabakon, kiu estas malvaste parenca kun la kuba kongao kaj devenas de afrikaj mantamburoj. Intertempe la kongao ankaŭ en Brazilo estas vaste disvastiĝinta. *Samba de roda* estas samtempe cirklodanco. Kie afrikobrazila kulturo intermiksiĝas kun indiĝenaj influoj, oni uzas la terminon *caboclo*, kiu ankaŭ signifas miksrasuloj kun indianaj prauloj. Tia variaĵo estas la *samba de caboclo*. Fakte la influo de indiĝena muziko sur la sambon tamen estas tre malgrava.
 
*Samba enredo* estas la moderna sambo de la karnavalo en Rio-de-Ĵanejro kaj São Paulo, sed ankaŭ je la karnavalo en Recife kaj en aliaj urboj. Ekde pluraj jardekoj en samba enredo fariĝas evoluo, argordi la instrumentojn pli alten kaj ludi la sambon pli kaj pli rapide. Tiun ĉi tendencon ebligis, ke ekde la 1970-aj jaroj oni anstataŭis la pli frue kutimajn naturfelojn per nilonfeloj. Krom tio je la karnavala parado nur laŭtaj muzikinstrumentoj estas aŭdeblaj sen elektra plilaŭtigo, tiel ke pandero kaj la frototamburo kuiko en la grandaj sambolernejoj ne plu estas ofte troveblaj. Superregas repinikoj, *caixas* (knartamburoj), surdoj agordataj laŭ tri malsamaj tonaltoj, tamburinoj, agogooj, ĉokaljoj (skuotuboj) kaj la fajfilo apito. Abunde ornamitaj dancistaroj ĉiam antaŭiras la muzikgrupojn (*baterias*), kiuj povas ampleksi plurcent muzikistojn.
 
*Samba canção* estas plejofte malrapida, baladeca kantovariaĵo de sambo, kiu estis populara precipe en la 1940-aj ĝis 1960-aj jaroj. Aliaj variaĵoj estas *sambolero*, miksaĵo el sambo kaj bolero, *samba-choro* aŭ *samba de breque* [bremsa sambo].
 
Pli nova pluevoluo estas la sambofunko respektivefunkeado. La plej novaj muzikstiloj de la 1990-aj jaroj enprenis fortajn influojn de funko same kiel de repo kaj kunfandis tiujn ĉi stilojn kun sambo. Famaj bandoj el Rio-de-Ĵanejro estas Funk'n Lata, kies muzikistoj lernis en la sambolernejo Mangueira, same kiel Monobloco.
 
En Salvador de Bahio evoluis el la tiea sambotradicio kaj influoj de regeo ekde la 1970-aj kaj 1980-aj jaroj la samboregeo. Tiun stilon kutime oni ne plu konsideras kiel sambostilo. Famaj samboregeaj muzikistaroj estas Olodum, Timbalada aŭ la pli tradiciaj Ilê Aiyê. Rio-de-Ĵanejro kaj San-Paŭlo estas la du centroj kaj de sambo kaj ankaŭ de la brazila amuzekonomio kaj krome la ekonomiaj centroj de la lando. Pro ĉi tiuj kialoj la muziko el tiu ĉi regiono disradias sur tutan Brazilon. Tio validas ankaŭ por la sambomuziko, kiu pro tio estas konsiderebla kaj kiel regiona muziko kaj ankaŭ kiel superregiona muzikstilo. Samboformoj ekzistas en preskaŭ ĉiuj regionoj de Brazilo; ekzemple en la karnavalo de Recife aŭ en tiu de Olinda en Pernambuko prezentas sambogrupoj. Samboinfluoj estas distingeblaj je malsamaj ampleksoj krom tio en preskaŭ ĉiuj stiloj de Muziko Populara Brazila (MPB).
 
Tradicie en la afrikobrazila kulturo danco estas fiksa ero de la muzikkulturo, ambaŭ do pro tio ne estas disigeblaj.[8] Ankaŭ dum la *desfile* [parado] en la karnavalo estas neimageble, ke tamburistaro prezentus sen dancistaro. En la sambolernejoj la muzikaj kaj la dancaj ensembloj estas egalrajtaj.
 
La tre simpligita formo de la sambodanco, kiu atingis en la 1950-j jaroj la dancolernejojn en Eŭropo kaj Nordameriko kaj je 1959 eniĝis en la turniran programon de la latinidaj dancoj, havas preskaŭ nenion komunan kun la origina brazila dancformo.
 
Komence de la dudeka jarcento sambo signifis simple *festaĉo.* La plej verŝajna etimologio estas la vorto *samba* el Kasanĝeo, kiu signifas *preĝa kunveno.* Dum la preĝaj kunvenoj, la nigraj sklavoj kantis kaj la blankuloj pensis, ke ili festas. Oni uzis Ĵongo-ritmon.
 
La ritmo de sambo estas dutempa kaj multe sinkopa. Kutima instrumentoj por ludi sambon estas tamburino kaj aliaj tamburoj, *kavakinjo* kaj gitaro. Apito, esperante ankaŭ *sambofajfilo*, estas tritona fajfilo uzata kiel signalilo en brazila sambomuziko. Oni uzas ĝin por marki la ritmon.
 
La sukceso de sambo en Eŭropo kaj Japanujo nur konfirmas ĝian kapablon konkeri fervorulojn, sendepende de ilia poa lingvo. Aktuale ekzistas centoj da sambolernejoj nur en Eŭropo (dissemitaj en landoj kiel Germanujo, Belgujo, Nederlando, Francujo, Svedujo, Svisujo).
 
Diante dessa realidade, Sambo deixa várias recomendações, desde a necessidade de o Governo equipar-se de condições necessárias para colectar, como deve, as receitas em megaprojectos, capacitando os técnicos, até a diversificação na atracção de investimentos. De entre vários outros sectores, aquele pesquisador defende a aposta na indústria manufactureira, capaz de gerar renda não só aos investidores, mas também aos moçambicanos no processo de transformação dos recursos naturais brutos.
 
Como se indica, tanto Presidente Guebuza como o então Ministro da Defesa, concordavam na necessidade de criação da EMATUM e MAM, justificando-a por conta de ameaças à segurança da República de Moçambique, quem então seria o responsável para o esclarecimento dos contornos da dívida? Seja como for, se Guebuza ganhasse as eleições como independente, estaria seguro pelas imunidades emanadas para o Presidente da República e Filipe Nyusi gozaria do actual estatuto para não ser imediatamente responsabilizado: por que razão os eleitores deixariam de votar em Guebuza em detrimento de Nyusi ou vice-versa? Se Guebuza concorresse como independente, se pode presumir um equilíbrio entre ambos e vitória de um ou outro, será que Guebuza não poderia evocar seus feitos pr