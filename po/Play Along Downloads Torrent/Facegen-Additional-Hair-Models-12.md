## Facegen Additional Hair Models 12

 
  
 
**LINK ····· [https://vercupalo.blogspot.com/?d=2tENy8](https://vercupalo.blogspot.com/?d=2tENy8)**

 
 
 
 
 

 Requirements
 Overview
 Mesh Creation Guidelines
 Seleting a Project
 Head Model Preparation
 Integration Setup
 Model Integration
 Tips & Tricks
 Technical Details
 Also see the FaceGen FAQ. 1. Requirements

- P4 or equivalent running Windows XP/Vista/7.
- 1GB or higher RAM.
- 10GB of hard drive space per project.

 Setting up a project requires an overnight run on your PC. 2. Overview All operations in Customizer take place in a **project**. A project creates a **model set**; a group of face-related models that fit together as seen in FaceGen Modeller: Every project has a **mean face**(created by Customizer), which other components should conform to as closely as possible (of course you may also want to ensure that some components fit with each other): You can integrate any number of meshes (heads, faces, eyes, teeth, hair, etc.) into the same project as long as:
1. They all fit to the same mean face.
2. The neck seams to be held fixed are in the roughly the same place.

 If your meshes fit together seamlessly when you create them, they will fit seamlessly for any face you create in FaceGen. You work with a project in 3 **stages**. The 'Head Model Preparation' and 'Integration Setup' stages set up the project, and the 'Model Integration' stage is where you actually integrate your meshes into FaceGen. Unless otherwise noted, you can exit Customizer at any time and continue your work later. 3. Mesh Creation Guidelines In the steps outlined later in this document, you should create your meshes according to the following guidelines: All Meshes:
- Polygonal models (meshes) only.
- Objects using multiple texture maps will be separated into separate objects - one per texture map.

Meshes With Morph Targets / Blendshapes / Endomorphs
- To have your morph targets appear in FaceGen, create them along with your base model - you can use ours as a guide. FaceGen morph targets are mesh-specific and are not copied over automatically.
- There is no need to include UVs on the morph target models - UVs are taken from the base model only.

Head Meshes Almost any mesh and UV layout for head models is acceptable, with the following restrictions:
- The mouth must be closed. You can add open-mouth animation morphs later, but the basic face shape statistics are defined with a closed mouth.
- The UV layout must be non-overlapping in the face area. In other words, a pixel in the face area of the UV map should not map to more than one point on the model. No mirrored face textures !
- You will have multiple models representing the face if:
    - You have several level-of-detail (LOD) models of a head.
    - You have several versions of a head model with different hairstyles or facial hair.
- For best results, your neck seam should not be close to the head (leave room below the chin for a double chin).
- The nostrils should not be indented for texture map creation. If you want indented nostrils, you'll have to integrate two versions and combine (see details in Integration below).
- SDK Use: To maximize speed and minimize RAM usage of the color texture statistics, you need to minimize the size of the texture statistics. This should be done by having the head in its own texture image, not shared with the body, and using the full extent of that texture image to maximize the size of the face area.

4. Creating a Project
- Select a directory with at least 10GB of free disk space. A new directory for your project will be created there. Do NOT use the default selected directory in 'program files' as this can cause issues with Windows Vista and 7.
- For simplicity, use a single unique name for the new project and it's model set name.
- If you've already created a project, you should be able to load it by navigating to the directory in which you created it.
- To modify the FaceGen Default model set, create a new project and watch for specific instructions below.

 5. Head Model Preparation **WARNING:**If you have previously completed the Head Model Preparation stage and wish to make changes, create a NEW project, do not use the 'back' buttons to redo this stage, as Customizer may produce incorrect results. Manual registration Customizer indicates the filename of the FaceGen internal mean face in both Wavefront OBJ and Maya ASCII (ma) formats. Import the internal mean face into your preferred 3D modelling application:
- Maya: do NOT use the OBJ file, use the MA file (or Maya will scramble the vertex ordering).

Feel free to rotate, move or scale the internal mean face to your preferred coordinate system, but do NOT alter its shape or mesh structure (eg. vertex ordering) in any way. Now you can build or modify your meshes to conform to the internal mean face as closely as possible. In other words, any head meshes must be modified to have the same shape as the internal mean face (in your chosen size, orientation and position) over its entire surface. When Customizer creates the shape and colour statistics for your meshes it will do so based on their positioning relative to the mean face. Your first step will be to build your highest resolution head mesh according to the 'Mesh Creation Guidelines' above. This will be used in the next stage to define the neck seam. If you only want to modify the existing FaceGen Default Model set, the rotated/scaled internal mean face for that has been placed in the subdirectory 'DefaultModelSetInternalMean' in your Customizer 1.3 directory. Use this without modifying it in any way. To get the hires base mesh, export the hires skin mesh from the default model set using FaceGen Modeller, but be sure to hit the 'set all to zero' button in both the symmetric and asymmetric shape tabs first ! Use this for your highest resolution head mesh below. Check registered models Export your transformed mean face and highest resolution head mesh. Your mesh(es) must be exported into the OBJ format:
- Maya: The OBJ export plugin can be loaded by selecting:
 Window -> Settings/Preferences -> Plug-in Manager.
 Be sure to freeze any transforms into the vertex coordinates before exporting.

This step lets you double-check that you haven't accidentally further transformed one of your meshes during import or export. In the first dialog box, select the OBJ of your transformed internal mean face. In the second dialog box, add the one or more models corresponding to your high-resolution head mesh. Your head mesh, shown in transparent green, should conform closely with the internal mean face, shown in opaque white. If you can't see any green at all, your mesh is probably in the wrong location, or shrunk to an imperceptible size. You will have to correct this problem before integration will work. 6. Integration Setup Obtain neck seam vertices Here you tell Customizer where the neck seam of your model is. The neck seam is important since customizer must keep this region fixed for any shape of face created in FaceGen when it creates the shape statistics. You can define the neck seam automatically in some cases by toggling through seams in the geometry, or you can define it manually, by selecting each vertex of the seam. You do not need to worry about duplicate vertices in the neck seam at this stage. You do not have to define a neck seam, but if you don't then the neck seam will change shape along with the head when you apply different faces to it with FaceGen. Don't worry if the head appears facing the wrong direction in this step; as long as this mesh overlapped closely in the 'Check registered models' step, your coordinate system has been properly registered. Create cache files Be sure you have 10GB of free disk space, and you don't need your computer's full power for the next 10-20 hours, then hit 'Start Setup'. Once it's finished, your project is set up and ready to integrate as many meshes as you need by repeating the Model Integration steps below. You should NEVER repeat this step unless you change the co-ordinate system of your meshes or the general location neck seam, in which case you will need to create a new project. Do not delete the 10GB of cache files if you think you may have to integrate more models in the future - or you will have to re-create them. 7. Model Integration The setup stages are only done once per project, but the model integration stage can be repeated as many times as you like as you add models to a model set. You do NOT need to repeat the setup stages if you modify your head meshes - only if your coordinate system changes or your neck seam changes significantly. All models to be part of the model set must go through the geometry integration step, even if you'll only be using the color texture stats. If your model has a UV map which includes some of the face or skin area, then it will need to be put through the texture integration step as well. Even if it doesn't, you may wish to perform this step to integrate your own texture image so it can be seen in FaceGen Modeller. Once you have integrated the models you want, they're ready to use in the directory 'XXX\csamYYY', where XXX is the data directory you selected for this project, and YYY is the project name you chose. To make this project appear as a model set in FaceGen Modeller:, just copy the 'csamYYY' directory (including its contents) into your FaceGen Modeller Model Set directory:
- Modeller 3.6 and later:
    - Create a subdirectory 'MyName' in your model set directory: Documents/FaceGen/Model Sets/.
    - Create a subdirectory within that 'MyName' giving: Documents/FaceGen/Model Setes/MyName/MyName/
    - Copy the 'csamYYY' contents into this directory
    - For each '.fr3d' file, create a similarly named '.txt' file containg the name of the part as you want it to appear in Modeller.
- Modeller 3.5:
    - Copy the 'csamYYY' direct