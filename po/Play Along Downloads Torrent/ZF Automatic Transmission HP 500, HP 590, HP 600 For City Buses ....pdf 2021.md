## ZF Automatic Transmission HP 500, HP 590, HP 600 For City Buses ....pdf

 
  
 
**LINK ››››› [https://tinurli.com/2tzdj0](https://tinurli.com/2tzdj0)**

 
 
 
 
 
# ZF Automatic Transmission HP 500, HP 590, HP 600 for City Buses: A Review
 
If you are looking for a reliable, efficient and comfortable automatic transmission system for your city bus fleet, you might want to consider the ZF HP 500, HP 590 or HP 600 series. These are high-performance transmissions designed specifically for urban and intercity buses, with features that enhance fuel economy, safety and passenger comfort.
 
The ZF HP 500, HP 590 and HP 600 transmissions are based on the proven ZF Ecomat technology, which has been used in more than one million buses worldwide. The Ecomat system uses a hydrodynamic torque converter to provide smooth and seamless gear shifts, reducing wear and tear on the engine and driveline. The system also adapts to the driving conditions and the driver's style, optimizing the performance and efficiency of the bus.
 
The ZF HP 500, HP 590 and HP 600 transmissions have six forward gears and one reverse gear, with a maximum input torque of 500 Nm, 590 Nm and 600 Nm respectively. They can handle engines with up to 260 kW of power and buses with up to 18 tons of gross vehicle weight. The transmissions are compatible with various types of engines, including diesel, natural gas, hybrid and electric.
 
One of the main advantages of the ZF HP 500, HP 590 and HP 600 transmissions is their low fuel consumption. The transmissions have an intelligent electronic control unit that monitors the engine speed, load and temperature, and selects the optimal gear ratio for each situation. The transmissions also have a lock-up clutch that engages at low speeds, reducing the slip losses in the torque converter. Additionally, the transmissions have an eco mode that lowers the engine speed and shifts gears earlier, saving up to 10% of fuel compared to conventional transmissions.
 
Another benefit of the ZF HP 500, HP 590 and HP 600 transmissions is their high level of safety. The transmissions have a brake interlock function that prevents the bus from moving when the brake pedal is pressed. The transmissions also have a hill start aid function that prevents the bus from rolling back when starting on an incline. Furthermore, the transmissions have a retarder function that provides additional braking power when needed, reducing the stress on the service brakes and increasing their lifespan.
 
Finally, the ZF HP 500, HP 590 and HP 600 transmissions offer a comfortable ride for both the driver and the passengers. The transmissions have a smooth start function that eliminates jerks and jolts when accelerating from a standstill. The transmissions also have a noise reduction function that lowers the engine noise level inside the bus. Moreover, the transmissions have a vibration damping function that reduces the vibrations transmitted to the chassis and the body of the bus.
 
In conclusion, the ZF HP 500, HP 590 and HP 600 transmissions are ideal solutions for city buses that require high performance, low fuel consumption and high comfort. The transmissions are based on the proven ZF Ecomat technology that has been trusted by bus operators around the world for decades. The transmissions are also easy to install, maintain and operate, making them a smart choice for your bus fleet.
 842aa4382a
 
 
