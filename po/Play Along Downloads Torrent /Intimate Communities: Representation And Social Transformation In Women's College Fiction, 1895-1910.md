## Intimate Communities: Representation And Social Transformation In Women's College Fiction, 1895-1910

 
  
 
**Download ————— [https://shurll.com/2tEhkJ](https://shurll.com/2tEhkJ)**

 
 
 
 
 
The terms "smashing" and "crushes," in the context of women's behavior from the late 19th century to the early 20th, refers to a practice of same-sex courting of classmates. The terms were also used to describe particularly intimate or romantic relationships between two women. Smashing has been an especially elusive and cryptic topic for researchers. Below is a selection of secondary and primary sources that are helpful starting points for researching student and faculty same sex relationships at Vassar and women's colleges more broadly in the late 19th and early 20th centuries.
 
Gaffney, M. L. (2018). *The intimate collaborations of female faculty in select women's colleges, 1890-1930: Women's early efforts to create the closet.*(Dissertation examining the relationships of Mary Emma Woolley and Jeannette Augustus Marks of Mount Holyoke College, and Katharine Lee Bates and Katharine Ellis Coman of Wellesley College.)
 
In the later half of the nineteenth century, coinciding with first wave feminism, several women's colleges were established in the Northeastern United States, now known collectively as the Seven Sisters colleges. Other universities went co-ed, and many newer public universities were founded specifically as co-educational institutions. The golden age of women's colleges, from the late 1800s through the first decades of the 1900s, corresponded with progressive attitudes about women's abilities, rights, health, and variety of social issues. College life and the all-American college girl in particular became pop culture phenomena, and was recognizable branding during this era.
 
College as it was portrayed in the college novel provides an inclusive, safe, isolated community of young women, a world that, as Shirley Marchalonis writes in her 1995 critical text College Girls: A Century in Fiction, exists "between girlhood and womanhood", and gave the feeling that college would extend girlhood a little longer. College has different expectations than the outside world, and Marchalonis notes that "women [in college] are to find and respect themselves, to value and use their abilities".2 Accordingly, in the college novel, college was also a place of transformation and possibility: a poor girl can become respectably middle-class via hard work and education, a bad girl straightens out with the inspiration and friendship of the all-around-girl heroine, an orphan finds a parent or parents, and all of the girls learn important lessons. College in fiction especially reflects new spaces and new roles for women at that time. Many of the girls on campus are active in social work, teaching, business and other career-oriented jobs, student politics, and even world travel.
 
One of the other specific meal sources arising in large part from college girl culture is the American version of the tea room. Tea rooms in general are decidedly twentieth century, since, before that, tea was had at home, in private domestic space. After the turn-of-the-century, however, several things combined to make tea rooms so popular that the "tea room era" lasted for over fifty years in America. With more girls going away to college combined with the modern event of the automobile road trip, like 'Sunday drives', tea rooms popped up on country roads along the way and department stores put in tea rooms for shoppers. Prohibition in the 1920s resulted in hotels hosting afternoon 'tea dances' as alternative venues for socializing. Tea rooms flourished in particular near women's colleges where a mother's home cooking was no longer available and male-centric dining clubs were not an option. The colleges often provided lists approved establishments, and girls gravitated toward these comfortable, home-like spaces for their meetings, or when they missed the dinner seating at the campus rooming house or dining-hall.
 
This paper represents a part of my process of experiencing firsthand the catastrophic events of September 20, 2017, the day Hurricane Maria made landfall in Puerto Rico. The hurricane interrupted my doctoral research on the social uses of the marquesina, or open aired carports in Puerto Rico, along with every other aspect of daily life in the island. This paper attempts to address how community members shifted their relationship to space in order to survive, when the geography radically changed at both at the largest and most intimate of scales. Using ethnographic observation and documentation during this time of crisis, I was able to gain a snapshot of how the reactivation of bygone traditions, ingenuity, and the transformation of domestic spaces were used as tools for survival within the community I was living with.
 72ea393242
 
 
