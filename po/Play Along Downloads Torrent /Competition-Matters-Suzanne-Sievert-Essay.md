## Competition Matters Suzanne Sievert Essay

 
 ![Competition Matters Suzanne Sievert Essay](https://wako-dou.sakura.ne.jp/sblo_files/wakodo-item/image/vt-2.jpg)
 
 
**Download File ⏩ [https://climmulponorc.blogspot.com/?c=2tCKZk](https://climmulponorc.blogspot.com/?c=2tCKZk)**

 
 
 
 
 
# Why Competition Matters: A Review of Suzanne Sievert's Essay
  
Competition is often seen as a negative or harmful aspect of life, especially for children. Many parents and educators try to avoid or minimize competition among children, fearing that it will damage their self-esteem, discourage their creativity, or foster aggression. However, in her essay "Competition Matters", Suzanne Sievert argues that competition is actually beneficial for children and adults alike. She claims that competition motivates us to improve ourselves, teaches us valuable lessons, and prepares us for the challenges of the real world.
  
In this article, I will summarize and evaluate Sievert's main points, as well as provide some suggestions on how to foster healthy and positive competition in our lives.
  
## Summary of Sievert's Essay
  
Sievert begins her essay by recounting an incident that happened when her five-year-old son participated in a pumpkin-decorating contest at his school. He had put a lot of effort and creativity into his pumpkin, which had a wild combination of carving, paint, and feathers. However, when the judges announced the results, they gave every entry the same color ribbon, implying that everyone was a winner and had done a great job. Sievert was worried that this approach was sending the wrong message to the children, that losing was something to be avoided or denied, rather than accepted and learned from.
  
She then observes that this trend of avoiding competition or declaring everyone a winner was prevalent in many aspects of society. She gives an example of a board game that she bought for her children, which had no potential loser or winner. The game quickly lost its appeal and excitement, as there was no incentive to keep playing or improving. She argues that competition is symbiotic with motivation, and that without competition, we lose our drive to excel and achieve.
  
She also points out that competition is not only fun and exciting, but also educational and beneficial. She says that competition teaches us important skills and values, such as sportsmanship, resilience, perseverance, problem-solving, and teamwork. She illustrates this with an example of how she races her children to see who can get dressed first in the morning. She praises the child who accepts defeat gracefully and calls him or her a "good sport". She says that this attitude of being a good sport is something that we need to foster in our children, as it will help them cope with failures and setbacks in life.
  
She also asserts that competition prepares us for the realities of the adult world, where we have to face challenges and competition in various fields and situations. She says that competition helps us discover our strengths and weaknesses, as well as explore different alternatives and strategies to improve ourselves. She gives an example of how her son learned from his loss in the pumpkin-decorating contest and decided to improve his skills for the next year. He also chose his own winner among the entries, which was a snowman made of pumpkins. He accepted his defeat and admired the creativity of the other child.
  
She concludes her essay by stating that competition is not something that we should fear or avoid, but rather embrace and enjoy. She says that competition is not harmful or detrimental for children or adults, but rather beneficial and rewarding. She says that competition matters because it motivates us, teaches us, and prepares us for life.
  
## Evaluation of Sievert's Essay
  
Sievert's essay is well-written and persuasive. She uses personal anecdotes and examples to support her claims and make her points relatable and engaging. She also uses rhetorical devices such as repetition, contrast, and questions to emphasize her arguments and appeal to the readers' emotions and logic. She organizes her essay clearly and coherently, with an introduction that states her thesis, body paragraphs that develop her main points, and a conclusion that summarizes her main points and restates her thesis.
  
However, Sievert's essay also has some limitations and weaknesses. She does not provide any evidence or sources to back up her claims or counter possible objections or alternative views. She relies mostly on her own opinions and experiences, which may not be representative or generalizable to other situations or contexts. She also does not acknowledge or address any potential drawbacks or disadvantages of competition, such as stress, anxiety, cheating, violence, or inequality. She presents a
 842aa4382a
 
 
