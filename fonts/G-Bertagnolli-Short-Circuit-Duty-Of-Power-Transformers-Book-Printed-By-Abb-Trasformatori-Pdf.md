## G Bertagnolli Short Circuit Duty Of Power Transformers Book Printed By Abb Trasformatori Pdf

 
 ![G Bertagnolli Short Circuit Duty Of Power Transformers Book Printed By Abb Trasformatori Pdf](https://i.imgur.com/zOBbgus.jpg)
 
 
**LINK ➡ [https://walllowcopo.blogspot.com/?download=2tytY4](https://walllowcopo.blogspot.com/?download=2tytY4)**

 
 
 
 
 
# The ABB Approach to Short-circuit Duty of Power Transformers: A Book Review
 
Short-circuit duty of power transformers is a topic of great importance for the design, operation and maintenance of electric power systems. It refers to the ability of a transformer to withstand the mechanical and thermal stresses caused by short-circuit currents in its windings. A transformer that fails to perform its short-circuit duty can cause serious damage to itself and other equipment, as well as endanger human lives and the environment.
 
One of the most comprehensive and authoritative books on this subject is *The ABB Approach to Short-circuit Duty of Power Transformers*, written by Giorgio Bertagnolli, a senior engineer at ABB Trasformatori, one of the world's leading manufacturers of power transformers. The book, first published in 1996 and revised in 2007, presents the theoretical and practical aspects of short-circuit duty of power transformers, based on the extensive experience and research of ABB.
 
The book covers topics such as:
 
- The transformer equivalent circuit and its parameters
- The nature and calculation of short-circuit currents and impedances
- The electromagnetic forces and deformations created by short-circuit currents
- The thermal effects and temperature rise due to short-circuit currents
- The design criteria and methods for ensuring short-circuit withstand capability
- The testing procedures and standards for verifying short-circuit performance
- The diagnosis and repair of short-circuit damage

The book is written in a clear and concise style, with numerous examples, figures, tables and references. It is intended for engineers, technicians, students and researchers who are interested in learning more about the short-circuit duty of power transformers. It is also a valuable reference for utilities, manufacturers, consultants and regulators who are involved in the planning, operation and maintenance of electric power systems.
 
The book can be obtained from ABB Trasformatori or from online sources such as Google Books or Scribd. However, it is not available as a free pdf download. The book is printed by ABB Trasformatori in Italy and has 227 pages. The ISBN number is 978-88-900442-9-2.
  
The book is divided into eight chapters, each covering a specific aspect of short-circuit duty of power transformers. The first chapter introduces the basic concepts and definitions of short-circuit duty, such as short-circuit current, short-circuit impedance, short-circuit power and short-circuit voltage. The second chapter explains the transformer equivalent circuit and its parameters, such as leakage reactance, magnetizing reactance, winding resistance and core losses. The third chapter describes the nature and calculation of short-circuit currents and impedances, taking into account the effects of system configuration, transformer connection, tap changer position and saturation.
 
The fourth chapter deals with the electromagnetic forces and deformations created by short-circuit currents in the transformer windings and core. It presents the analytical and numerical methods for estimating the forces and deformations, as well as the design criteria and measures for reducing them. The fifth chapter discusses the thermal effects and temperature rise due to short-circuit currents in the transformer windings and oil. It shows how to calculate the temperature rise and the hot-spot temperature, as well as the design criteria and methods for ensuring adequate cooling and insulation.
 
The sixth chapter covers the design criteria and methods for ensuring short-circuit withstand capability of power transformers. It explains how to select the appropriate materials, dimensions, clearances, clamping and bracing systems for the transformer components. It also describes the standards and specifications for short-circuit duty of power transformers, such as IEC 60076-5 and IEEE C57.12.00. The seventh chapter describes the testing procedures and standards for verifying short-circuit performance of power transformers, such as IEC 60076-5 and IEEE C57.12.90. It explains how to perform short-circuit tests, how to measure and evaluate the test results, and how to diagnose and repair any damage caused by short-circuit currents.
 
The eighth chapter summarizes the main conclusions and recommendations of the book. It also provides some suggestions for future research and development on short-circuit duty of power transformers. The book is complemented by four appendices that provide additional information on topics such as transformer connections, equivalent circuits, forces calculation and testing equipment.
 842aa4382a
 
 
