## Narayan Bali Puja Vidhi Pdf Free

 
  
 
**LINK &gt;&gt;&gt; [https://walllowcopo.blogspot.com/?download=2tAgET](https://walllowcopo.blogspot.com/?download=2tAgET)**

 
 
 
 
 
# How to Perform Narayan Bali Puja for Unnatural Death
 
Narayan Bali Puja is a Hindu ritual that is performed to appease Lord Narayana (Vishnu/Krishna) for the souls of those who died an unnatural death. Unnatural death can include suicide, murder, accident, disease, curse, animal attack, etc. According to the Garuda Purana, a sacred scripture that deals with the afterlife, such deaths cause the soul to wander as a ghost (preta) and suffer from hunger, thirst and pain. The Narayan Bali Puja helps to release the soul from this state and grant it peace and liberation.
 
In this article, we will explain the steps and benefits of performing Narayan Bali Puja for unnatural death. We will also provide some links where you can download free pdf files of the puja vidhi (procedure) in Sanskrit and Hindi languages.
 
## Steps of Narayan Bali Puja
 
1. The puja should be performed on the eleventh day after the death of the person or on any auspicious day thereafter.
2. The puja should be performed by the son or any male relative of the deceased under the guidance of a qualified priest.
3. The puja requires a sacrificial fire (homa), a clay idol of the deceased (pind), a black cloth (kafan), a bamboo pole (danda), a coconut (nariyal), rice grains (akshat), flowers, incense, lamps, water, milk, honey, ghee, etc.
4. The priest invokes Lord Narayana and other deities and requests them to accept the offerings and bless the soul of the deceased.
5. The priest then performs the homa with various mantras and offerings. He also performs a symbolic cremation of the clay idol by placing it in the fire.
6. The priest then performs a symbolic funeral procession by carrying the bamboo pole with the black cloth and coconut tied to it. He takes it to a river or any water body and immerses it there.
7. The priest then performs a symbolic shraddha (memorial service) by offering rice balls (pindas) to the ancestors and feeding Brahmins (priests).
8. The priest then gives some charity (dana) to the poor and needy in the name of the deceased.
9. The priest then concludes the puja by thanking Lord Narayana and other deities and asking for their forgiveness and blessings.

## Benefits of Narayan Bali Puja

- The puja helps to free the soul of the deceased from the bondage of ghosthood and grants it peace and liberation.
- The puja helps to remove the negative effects of unnatural death on the family and relatives of the deceased.
- The puja helps to prevent diseases, accidents, misfortunes and enemies that may be caused by the restless soul of the deceased.
- The puja helps to earn merit (punya) for both the performer and the deceased.
- The puja helps to please Lord Narayana and other deities and receive their grace and protection.

## Links for Free PDF Files of Narayan Bali Puja Vidhi
 
If you want to learn more about Narayan Bali Puja Vidhi or perform it yourself, you can download free pdf files of the procedure in Sanskrit and Hindi languages from these links:

- [Sugam Narayan Bali Shraddha Paddhati by Karmakanda Sarathi](https://archive.org/details/SugamNarayanBaliPaddhati)
- [Sugam Narayan Bali Shraddha Paddhati Pdf by Online Sanskrit Books](https://www.onlinesanskritbooks.com/2019/03/sugam-narayan-bali-shraddha-paddhati-pdf.html)
- [Narayana Bali Puja (Offerings unto Lord Nar 842aa4382a




](https://dipika.org.za/wp-content/uploads/2015/04/9.Narayan-Bali-Rites-for-an-unnatural-death.pdf)