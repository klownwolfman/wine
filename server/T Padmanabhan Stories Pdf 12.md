## T Padmanabhan Stories Pdf 12

 
  
 
**## Links to get files:
[Link 1](https://urlca.com/2tBVan)

[Link 2](https://urluso.com/2tBVan)

[Link 3](https://tweeat.com/2tBVan)

**

 
 
 
 
 
# T Padmanabhan: A Master of Malayalam Short Stories
 
T Padmanabhan is one of the most acclaimed and influential writers of Malayalam literature. He has written more than 200 short stories, many of which have been translated into various languages and adapted into films and plays. His stories are known for their realistic portrayal of human emotions, social issues, and rural life in Kerala.
 
One of his most comprehensive collections is *T Padmanabhante Kathakal Sampoornam*, which contains 162 stories written by him up to 2002. The book also includes a study by M Thomas Mathew, a literary critic and scholar. Some of the stories in this collection are *Oru Kathakruth Kurisil* (A Story Writer on the Cross), *Kadal* (The Sea), *Kaalabhairavan* (The Black God of Time), *Gowri*, *Nidhichala Sukhama* (Is Happiness Permanent?), *Prakasam Parathunna Oru Pennkutty* (A Girl Who Shines Brightly), *Sekhootty*, and *Veedu Nashtappetta Oru Kutty* (A Child Who Lost His Home).
 
T Padmanabhan's stories are rich in symbolism, imagery, and irony. He uses simple language and narration to convey complex themes and messages. He explores the human condition in all its aspects, such as love, death, faith, morality, identity, and alienation. He also depicts the social and political changes that have affected Kerala over the decades, such as the land reforms, the communist movement, the Gulf migration, and the urbanization.
 
T Padmanabhan has won several awards and honors for his contribution to Malayalam literature, such as the Kendra Sahitya Akademi Award, the Kerala Sahitya Akademi Award, the Vayalar Award, the Ezhuthachan Puraskaram, and the Padma Shri. He is widely regarded as one of the finest short story writers in India and a pioneer of modern Malayalam fiction.
  
Some of the recurring themes in TP's stories are death, loneliness, memory, nostalgia, and the clash between tradition and modernity. He often portrays the lives of ordinary people who face extraordinary situations or dilemmas. He also experiments with different narrative techniques and styles, such as stream of consciousness, allegory, satire, and fantasy. His stories are marked by a subtle humor, a poetic sensibility, and a keen observation of human nature.
 
One of the most remarkable aspects of TP's stories is his ability to create vivid and memorable characters. He has a knack for capturing the essence of a person in a few strokes of words. His characters are not stereotypes or caricatures, but complex and nuanced individuals who reflect the diversity and contradictions of Kerala society. He has created memorable characters such as Gauri, the young widow who defies social norms and finds happiness in her own way; Harrison Saheb, the eccentric Englishman who loves Kerala and its culture; Makhan Singh, the Sikh soldier who sacrifices his life for his Malayali friend; Sekhootty, the Muslim boy who dreams of becoming a singer; and many others.
 
TP's stories are also notable for their cultural and historical references. He draws from the rich heritage of Kerala's folklore, mythology, art, music, and literature. He also incorporates elements from other cultures and traditions, such as Hindustani music, Russian literature, Western philosophy, and Buddhism. He also depicts the historical events and changes that have shaped Kerala's history, such as the freedom struggle, the Second World War, the Communist movement, the Emergency, and the Gulf migration.
 
TP's stories are not only entertaining and enlightening, but also inspiring and challenging. They invite the reader to reflect on their own values, beliefs, and choices. They also celebrate the beauty and dignity of life in all its forms. They are stories that transcend boundaries of time, space, and language. They are stories that deserve to be read and appreciated by a wider audience.
 842aa4382a
 
 
