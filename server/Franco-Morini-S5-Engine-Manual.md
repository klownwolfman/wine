## Franco Morini S5 Engine Manual

 
  
 
**CLICK HERE ->->->-> [https://ssurll.com/2tBO3y](https://ssurll.com/2tBO3y)**

 
 
 
 
 
# How to Maintain Your Franco Morini S5 Engine
 
The Franco Morini S5 engine is a two-stroke, single-cylinder engine that was used by many motorcycle manufacturers from the late 1960s to 2001. It is known for its unusual kick-start lever that goes forward instead of backward, and its reed valve intake system with three metal flaps. The S5 engine has a displacement of 49.908 cc, a bore and stroke of 39 x 41.8 mm, and a compression ratio of 11:1. It can reach a maximum speed of 9000 rpm and uses a 12 mm carburetor. The engine is air-cooled and has an automatic oil mixer pump for lubrication.
 
If you own a motorcycle with a Franco Morini S5 engine, you may want to know how to perform some basic maintenance tasks to keep it running smoothly and safely. Here are some tips and instructions based on the Franco Morini service manual[^2^]:
 
- Check the spark plug regularly and replace it if it is worn or damaged. The recommended spark plug is CHAMPION L78 C.
- Use only unleaded fuel with a minimum octane rating of 95 RON, mixed with 2% of AGIP 2T CITY oil or equivalent.
- Change the transmission gear oil every 1000 km or every six months. The recommended oil is AGIP RADULA 68 type SAE 20 W, and the required quantity is 200 cc.
- Clean the air filter every 500 km or more frequently if you ride in dusty conditions. Wash it with warm water and soap, rinse it well, and let it dry completely before reinstalling it.
- Adjust the clutch every 1000 km or whenever you notice slippage or noise. The clutch is located inside the right crankcase cover and consists of two shoes that are activated by centrifugal force. To adjust the clutch, loosen the lock nut and turn the adjusting screw until there is a slight clearance between the shoes and the drum. Then tighten the lock nut and check the operation of the clutch.
- Check the chain tension and alignment every 500 km or more often if you ride on rough terrain. The chain should have a slack of about 10 mm when measured at the midpoint between the sprockets. To adjust the chain tension, loosen the rear wheel axle nut and turn the chain adjusters on both sides of the swingarm until the desired tension is achieved. Then tighten the axle nut and make sure the wheel is aligned with the frame.

These are some of the most common maintenance tasks for your Franco Morini S5 engine. For more detailed information and instructions, you can refer to the original service manual[^2^] or consult a qualified mechanic. By taking good care of your engine, you can enjoy riding your motorcycle for many years.
  
## How to Troubleshoot Your Franco Morini S5 Engine
 
Sometimes, your Franco Morini S5 engine may not work properly or may fail to start. This can be caused by various factors, such as faulty components, incorrect settings, or poor maintenance. To diagnose and fix the problem, you can follow some simple troubleshooting steps based on the Franco Morini service manual:

1. Check the fuel level and quality. Make sure there is enough fuel in the tank and that it is not contaminated with water or dirt. If the fuel is old or stale, drain it and refill with fresh fuel.
2. Check the spark plug and ignition system. Remove the spark plug and inspect its condition. If it is dirty, fouled, or damaged, clean it or replace it with a new one. Also check the spark plug gap and make sure it is within the specified range of 0.6 - 0.7 mm. To test the spark, connect the spark plug to the high tension wire and hold it against the cylinder head. Then kick-start the engine and observe the spark. It should be strong and blue. If there is no spark or a weak spark, check the ignition coil, the wiring, and the generator.
3. Check the carburetor and fuel system. Make sure the air filter is clean and not clogged. Also check the fuel filter and the fuel hose for any blockage or leakage. If necessary, clean or replace them. Then check the carburetor settings and make sure they are correct for your altitude and climate. The main jet size should be between 60 and 70, depending on the temperature and humidity. The idle speed should be between 1800 and 2000 rpm, and the idle mixture screw should be turned out 1 - 1.5 turns from fully closed.
4. Check the compression and cylinder condition. To measure the compression, remove the spark plug and insert a compression gauge into the spark plug hole. Then kick-start the engine several times and read the gauge. The compression should be between 9 and 11 bar. If the compression is too low, check the piston rings, the cylinder bore, and the cylinder head gasket for any wear or damage.

If none of these steps solve the problem, you may need to consult a qualified mechanic for further assistance.
  
## How to Upgrade Your Franco Morini S5 Engine
 
If you want to improve the performance of your Franco Morini S5 engine, you can install some aftermarket parts or modify some of the existing components. However, you should be aware that doing so may void your warranty or affect your emissions compliance. You should also make sure that any modifications are legal and safe for your riding conditions. Here are some of the possible upgrades you can make to your Franco Morini S5 engine:

- Replace the stock exhaust system with a performance exhaust system. This can increase the power output and reduce the weight of your engine. However, you may also need to adjust the carburetor settings to match the new exhaust system.
- Replace the stock air filter with a high-flow air filter. This can improve the airflow and reduce the restriction to your engine. However, you may also need to adjust the carburetor settings to match the new air filter.
- Replace the stock cylinder with a big bore cylinder kit. This can increase the displacement and compression ratio of your engine, resulting in more power and torque. However, you may also need to replace other components such as the piston, rings, gaskets, spark plug, etc.
- Replace the stock ignition system with an electronic ignition system. This can improve the timing and reliability of your engine. However, you may also need to modify or replace other components such as the generator, coil, wiring, etc.

These are some of the most common upgrades you can make to your Franco Morini S5 engine. For more information and instructions, you can refer to online forums or guides from other enthusiasts who have done similar modifications.
 842aa4382a
 
 
