## Rimworld Run And Gun

 
 ![Rimworld Run And Gun](https://jawerontheweb.weebly.com/uploads/1/3/3/2/133211082/350306416_orig.jpg)
 
 
**DOWNLOAD » [https://bionallopi.blogspot.com/?file=2tB01g](https://bionallopi.blogspot.com/?file=2tB01g)**

 
 
 
 
 
# How to Use the Run and Gun Mod in RimWorld
 
RimWorld is a sci-fi colony simulator game that challenges you to survive and thrive on a hostile planet. You can customize your colonists, craft weapons and armor, build structures and defenses, and fight off raiders, pirates, mechanoids, and other threats.
 
One of the mods that can enhance your gameplay experience is the Run and Gun Mod by roolo. This mod allows your drafted colonists and NPC pawns to shoot while moving at the cost of an accuracy and movement penalty. This can give you more tactical options and make combat more dynamic and exciting.
 
In this article, we will explain how to use the Run and Gun Mod in RimWorld, what are its features and settings, and how to balance it with other mods.
  
## How to Install the Run and Gun Mod
 
The Run and Gun Mod requires HugsLib Library by UnlimitedHugs to work. You can download both mods from the Steam Workshop or from GitHub. To install them, follow these steps:
 
1. Subscribe to HugsLib Library and Run and Gun Mod on Steam Workshop or download them from GitHub.
2. Launch RimWorld and go to Mods menu.
3. Enable HugsLib Library and Run and Gun Mod by clicking on the checkboxes next to their names.
4. Make sure HugsLib Library is loaded before Run and Gun Mod in the mod order list. You can drag and drop them to change their positions.
5. Click on Restart Now button to apply the changes.

## How to Enable Run and Gun Mode
 
To enable Run and Gun Mode for your colonists, you need to draft them first by selecting them and pressing R key. Then, you will see a new button on the bottom right corner of the screen that looks like a running person with a gun. Click on it to toggle Run and Gun Mode on or off.
 
Note that Run and Gun Mode only works for colonists who are carrying ranged weapons in their hands. If they have melee weapons or no weapons at all, they will not be able to use it. Also, they need to have Fire at Will enabled, which is the default setting for drafted colonists. If you want them to fire manually, they will stop running when you click on a target.
 
When Run and Gun Mode is enabled, your colonists will shoot while moving at the cost of an accuracy and movement penalty. The accuracy penalty depends on how fast they are moving, while the movement penalty depends on how heavy their weapon is. You can see these penalties as red numbers next to their shooting accuracy and movement speed stats on the bottom left corner of the screen.
 
The penalties are only applied when your colonists are actually shooting or cooling down after shooting. So if you just want them to walk somewhere without firing, you don't need to disable Run and Gun Mode. Similarly, if they stand still while shooting, they will not suffer any accuracy penalty.
  
## How to Configure Run and Gun Settings
 
The Run and Gun Mod has several settings that you can tweak to your liking. To access them, go to Options menu and click on Mod Settings button. Then, select RunAndGun from the list of mods on the left side of the screen. You will see a number of sliders and checkboxes that you can adjust.
 
Here are some of the settings that you can change:

- Accuracy penalty: This determines how much accuracy your colonists lose when they run and gun. You can set it from 0% (no penalty) to 100% (no accuracy).
- Movement penalty: This determines how much movement speed your colonists lose when they run and gun. You can set it from 0% (no penalty) to 100% (no movement).
- Movement penalty heavy weapon: This determines how much movement speed your colonists lose when they run and gun with a heavy weapon. You can set it from 0% (no penalty) to 100% (no movement).
- Movement penalty light weapon: This determines how much movement speed your colonists lose when they run and gun with a light weapon. You can set it from 0% (no penalty 842aa4382a




