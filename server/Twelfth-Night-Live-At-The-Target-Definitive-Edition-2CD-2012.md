## Twelfth Night - Live At The Target (Definitive Edition 2CD) 2012

 
  
 
**## File download links:
[Link 1](https://bytlly.com/2tFjwy)

[Link 2](https://geags.com/2tFjwy)

[Link 3](https://urllie.com/2tFjwy)

**

 
 
 
 
 
No one ever expected Clive to come back to the UK, and even when he did there was no certainty the band would reform, but reform they did (without original keyboard player Rick Battersby), and since then there have been quite a few trips down memory lane with members of Galahad subbing in at different times. But all good things come to an end and Andy Revell wanted to go out at a big event, and so Barbican's Sill Street Theatre was booked and on Saturday 15th December 2012 the band played the final (?) gig. The line-up featured three guys who had been there at the very beginning, namely Brian Devoil (drums), Andy Revell (guitar, backing vocals) and Clive Mitten (bass, guitar, keyboards, backing vocals). They were joined by Dean Baker on keyboards and piano, who had been a constant presence since the band reformed, along with 'new' singer Mark Spencer who also provided some guitar. Both Dean and Mark are also full-time members of Galahad (plus other bands), while Roy Keyworth, who used to also be in both Twelfth Night and Galahad, joins the band for 'East of Eden'. As always, the band kicked off proceedings with 'The Ceiling Speaks' where Revell and Mitten duel on guitars, with bass coming from synths, and immediately they are up and running and the audience are in fine voice. All anyone really knows of the setlist at a TN concert is the opening song and the last, which will always be 'Love Song', so I was intrigued to see what was going to be included here and I guess I shouldn't have been surprised that there was a significant move away from material recorded by Andy Sears. Mark's voice is quite similar to Geoff's in many ways, so he would be more comfortable with the early songs, and perhaps that is why a decision was made to include just a couple of songs from 'Art and Illusion' and nothing at all from 'The Virgin Album'. It is a shame not to hear the drama of 'Blondon Fair' or 'Take A Look' but putting those to one side I think the only song of note not in this set would be 'The Collector'. We have time for 'We Are Sane', 'Sequences' and 'Creepshow' alongside the likes of 'Human Being' and 'Fact and Fiction'. By my reckoning this is the twelfth official live album from Twelfth Night (counting 'Live and Let Live' plus the 'Live and Let Live Definitive' albums as two), which somehow seems fitting, and yes I do have them all. Each one is a gem in its own right, a snapshot of time, and while I must confess this doesn't quite live up to Geoff's last album with them, that is less to do with the performance and more the raw emotion and passion from everyone knowing it was Geoff Mann's last ever gig with the band. This set has also been released on Blu-ray and DVD, but due to poor planning on my part I have ended up in one part of the country with my Blu-ray player in another, so that review will have to wait a few weeks. But, if you search for 'Twelfth Night A Night To Remember' on YouTube you will be able to see some clips from that, which proves just what a band this is/was. This can't be the end; we've already had a teaser with the 'Sequences' EP so let us see what happens next. Until then, listen to a modern version of one of the best prog bands ever to come out of Reading. social review comments  | Review Permalink
Posted Saturday, May 30, 2020 | Review this album | Report (Review #2408339)
 72ea393242
 
 
