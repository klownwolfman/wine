## Abel Gance Et Son Napoleon

 
 ![Abel Gance Et Son Napoleon](https://1.bp.blogspot.com/-T_I0zPHjdh8/UnO3qbmakpI/AAAAAAAADIs/UhX5Os_SHXc/s1600/screenshot_02.jpg)
 
 
**## Files available for download:
[Link 1](https://urlin.us/2tDGt0)

[Link 2](https://tlniurl.com/2tDGt0)

[Link 3](https://bltlly.com/2tDGt0)

**

 
 
 
 
 
In July 1793, fanatic Girondist Charlotte Corday (Marguerite Gance) visits Marat in his home and kills him with a knife. Two months later, General Jean François Carteaux (Léon Courtois), in control of a French army, is ineffectively besieging the port of Toulon, held by 20,000 English, Spanish and Italian troops. Captain Napoleon is assigned to the artillery section and is dismayed by the obvious lack of French discipline. He confronts Carteaux in an inn run by Tristan Fleuri, formerly the scullion of Brienne. Napoleon advises Carteaux how best to engage the artillery against Toulon, but Carteaux is dismissive. An enemy artillery shot hits the inn and scatters the officers. Napoleon stays to study a map of Toulon while Fleuri's young son Marcellin (Serge Freddy-Karl) mimes with Napoleon's hat and sword. Fleuri's beautiful daughter Violine Fleuri (Annabella) admires Napoleon silently.
 
Another restoration was made by Brownlow in 1983. When it was screened at the Barbican Centre in London, French actress Annabella, who plays the fictional character Violine in the film (personifying France in her plight, beset by enemies from within and without), was in attendance. She was introduced to the audience prior to screenings and during one of the intervals sat alongside Kevin Brownlow, signing copies of the latter's book about the history and restoration of the film.
 
Although the triumphant "Napoleon" release seemed to promise a belated Gance revival, it ended up doing much more for the cause of fresh presentations of silents in general than in bringing full recognition to its director. While reviewers in major dailies like Vincent Canby in "The New York Times" and Charles Champlin in "The Los Angeles Times" were rhapsodic in their appreciation of Gance and his film, there was a sharp reaction in a number of the journals, whether it was Pauline Kael, formerly a Gance supporter, rounding on him in "The New Yorker" as she had earlier done with Orson Welles in the belief that he was now "establishment," or Stanley Kauffmann in "The New Republic" taking the tack that yes, Gance had been unfairly neglected but his new admirers were starting to exaggerate his contributions. Worst of all, however, were the politically motivated attacks, starting with the right-wing magazine, "Commentary," which tried to brand "Napoleon" as an ultra-nationalist piece of propaganda in an effort to discredit as hypocrites its presumably liberal admirers among the New York intellectuals, such as Leonard Bernstein. "Cineaste" then published an article by Peter Pappas who continued the dirty work of Richard Grenier in "Commentary" by labeling "Napoleon" as a "fascist" film. That Pappas was merely using the word "fascist" in the abusive, meaningless sense as defined by George Orwell in "Politics and the English Language" in which "fascism" no longer meant anything other than "something not desirable" is, I think, self-evident. Nevertheless, the damage was done and the long night of Gance's journey back to virtual obscurity had soon begun. To be sure, there were no pickets showing up to protest showings of Gance's films, nor were there cinema guilds removing his name from awards given in his honor. The piece by Pappas in "Cineaste," followed a few years later by a similarly loaded political hatchet job by Norman King published by the British Film Institute, were in the nature of preemptive strikes before the director ever had the chance to obtain the recognition then accorded Griffith.
 72ea393242
 
 
