## Nude Recreation Week 2009

 
  
 
**## Download files here:
[Link 1](https://urluso.com/2tEB9k)

[Link 2](https://urlca.com/2tEB9k)

[Link 3](https://urlgoal.com/2tEB9k)

**

 
 
 
 
 
**Sunsets and the Green Flash:**
With a sharp horizon and a clear sky, it's possible to see the green flash following sunset. Once the sun gets low in the sky, refraction of our atmosphere separates colored images of the sun, we see the green image after the brighter, red image goes over the horizon. Watch for a moment of green light after sunset. 

Catalina is seen silhouetted by the setting sun late in May and mid mid-July, perhaps 11-14.

**Tar on the Beach:**
The currents sometimes wash tar up onto our beach this time of year. Some of us try to pick it up, but more keeps washing up. Watch your step and watch where you spread your towel. It's very hard to get it off. To remove it, use any oil-based product. Some people carry baby oil, or suntan oil just for that purpose. Even lip balm will work in a pinch. Because some people pick it up, it's less likely other people will step in it or ruin a towel. If you're going to pick it up, consider using a kelp leaf. 

**Stingrays and Jellyfish:**
There are stingrays in our water all year, even knee deep. Sting rays don't attack people, but if you step on one, you will learn that their defense mechanism is very painful. It's treated with heat. Bury the affected area in hot sand. The pain takes an hour or so to pass and rarely requires medical attention.

Jellyfish are seen washed up on the beach, but how many people give thought to the fact that there must also be some in the water. These animals are unable to see you and pursue you, especially near the shore. They are very much victims of water currents. If you see one in the water, note the direction the water takes them and keep out of the way. Even a dead piece of a jellyfish stings on contact.

If stung, don't spread it by scratching. Untreated, the itch passes quickly. Lifeguards spray on rubbing alcohol, but it's unclear that it actually helps. Rinsing with sea water may be just as effective.

**The Naturist Minority:**
We have only a few oases where clothing is optional. Without the Naturist Society (TNS), even these oases would not exist. Many of us feel secure on our oasis. Things have gone well here, but we can't count on that forever. We all hope for the best, but joining an organization is the first step in preparing for the worst. The best way to avoid a fight is to show that we're ready for anything.
National Nude Recreation Week:
National Nude Recreation Week is July 4-10 this year. To us here at Black's Beach, it means doing what we always do, enjoying our freedom to choose not to wear clothes. It's a wholesome freedom everyone should enjoy. 

It's a freedom our forefathers enjoyed, but is facing more challenges every year as our population grows. Will this freedom be reduced to just private property, and from there be restricted to the shower and bedroom?

The Naturist Society (TNS) has been instrumental in preserving nude recreation at Black's Beach, and Black's Beach Bares continues to work with TNS. Please join with us to ensure that there is always room on public land for nude recreation. Look for our yellow flag for more information.

**Red Tide:**
About this time of year we sometimes have red tide, giving our water a red or amber color and poor visibility. Dinoflagellates are the culprits. These plankton reproduce explosively.

The dinoflagellates here in San Diego are bioluminescent, meaning that they glow when turbulence is introduced. Each wave will give off light. If you swim at night, you will see a glow with every stroke. If you walk in the surf at night, every footstep will sparkle. If you can't be here at night, take some water home and shake it up in a dark room. 

**Donations:**
Many people take free copies from the bulletin board and the people who read newsletters from the website are uncounted too. These free sources of the newsletter will continue, because keeping people informed is the point.

There are monthly expenses for operating the website. Donations can be directed to Black's Beach Diggers for trail work. You decide if the newsletter, the website or the trail have made your visit better, and donate accordingly.
 
**From Black's Beach Diggers:**
We appreciate donations of lumber and cash. However, we ask that there be no further lumber donations. We already have more lumber than we con use, since we have no plans to improve the trail after mid-May. Lumber donations should be made much earlier in the year, like December - March.

Cash donations are still appreciated, as it will help us recoup our expenses. Lumber, tools, and hardware is bought out-of-pocket. The steel spikes alone cost $3-$4 each depending on length.

**Torrey Pines Reef 2:**
There is an artificial reef offshore, about 3/8 mile out and 45 feet down. This reef is host to a variety of life, fish, lobster and other invertebrates. Now that the water is warm, swim expeditions to the reef will take place in the early afternoon. Ambitious swimmers should look for buddies between our two flags, or contact Lloyd through the website.

**Tar on the Beach:**
The currents sometimes wash tar up onto our beach this time of year. Watch your step and watch where you spread your towel. It's very hard to get it off. To remove it, use any oil-based product, baby oil, suntan oil, or even lip balm. If you're going to pick it up, consider using a kelp leaf. 

**The Naturist Society (TNS):**
TNS is a member organization formed to promote nude recreation and body acceptance. They do this by defending clothing optional use of public land through participating groups like ours. Without their hard work in years past, you would not be nude on this beach or any other.

We constantly need your help. You say you've been coming to Black's Beach for how many years? Has it gotten better? Has it gotten worse? What have you done? What have you given back?

TNS membership is a good start. With TNS membership you get a subscription to their quarterly magazine, discounted admission to resorts, invitations to gatherings and festivals, discounts on Skinny Dipper merchandise and voting privileges. Most importantly your membership adds strength in numbers to protect our choice not to wear clothes. 

**Subscriptions:**
Subscriptions are $10/year. Please subscribe, renew or make a donation. I ask a $2 donation for email subscriptions.

Many people take free copies from the bulletin board and the people who read newsletters from the website are uncounted too. These free sources of the newsletter will continue.

Please consider making a donation anyway. There are monthly expenses for the website. Donations can also be directed to trail work. You decide if the newsletter, the website or the trail have made your visit better.
 
**The Naturist Minority:**
When we are surrounded by like-minded people, we forget that we are an oppressed minority. We have only a few oases where clothing is optional. Without the Naturist Society (TNS), even these oases would not exist. Many of us feel secure on our oasis. Things have gone well here, but we can't count on that forever. We all hope for the best, but joining an organization is the first step in preparing for the worst. The best way to avoid a fight is to show that we're ready for anything.

**The Naturist Society (TNS):**
TNS is a member organization formed to promote nude recreation and body acceptance. They do this by defending clothing optional use of public land through participating groups like ours. Without their hard work in years past, you would not be nude on this beach or any other.

We constantly need your help. You say you've been coming to Black's Beach for how many years? Has it gotten better? Has it gotten worse? What have you done? What have you given back?

TNS membership is a good start. With TNS membership you get a subscription to their quarterly magazine, discounted admission to resorts, invitations to gatherings and festivals, discounts on Skinny Dipper merchandise and voting privileges. Most importantly your membership adds strength in numbers to protect our choice not to wear clothes. 

**Flies:**
We are also entering the time of year when the flies can become a bother. When there is a lot of seaweed and no wind, they don't leave us alone. On those days you will find seaweed covered with flies that swarm every time anybody comes near. The remedy is to move the seaweed far from yourself, and out of the path of by passers. It's also a good idea to use some kind of repellent.

**Sunsets and the Green Flash:**
With a sharp horizon and a clear sky, it should be possible to see the green flash following sunset. Once the sun gets low enough in the sky and the refraction of our atmosphere separates colored images of the sun, we can see the green image after the brighter, red image goes over the horizon. Watch for a moment of green light after sunset. 

**Tar on the Beach:**
The currents sometimes wash tar up onto our beach this time of year. Some of us try to pick it up, but more keeps washing up. Watch your step and watch where you spread your towel. It's very hard to get it off. To remove it, use any oil-based product. Some people carry baby oil, or suntan oil just for that purpose. Even lip balm will work in a pinch. Because some people pick it up, it's less likely other people will step in it or ruin a towel. If you're going to pick it up, consider using a kelp leaf. 

**Trail and Cliffs:**
The trail suffered significant damage from recent storms, and there's sure to be more rain. Black's Beach Diggers is on the job keeping the trail open. Donations of tools, labor and new lumber are appreciated.

**Subscriptions:**
Subscriptions are $10/year. Please subscribe, renew or make a donation. I ask a $2 donation fo