## pictures of eragon the book

 
  
 
**## Files available for download:
[Link 1](https://bltlly.com/2tFsju)

[Link 2](https://urlin.us/2tFsju)

[Link 3](https://blltly.com/2tFsju)

**

 
 
 
 
 
Eragon (movie)Official Eragon trailerInfoYear2006Length1 hour, 39 minutesCharacters

- Eragon
- Arya
- Brom
- Durza
- Galbatorix
- Saphira

Actors
- Ed Speleers
- Jeremy Irons
- Sienna Guillory
- Garrett Hedlund
- Robert Carlyle
- etc

ReceptionDomestic Box Office$75,030,163International Box Office$249,488,115Rotten Tomatoes Rating16%**Eragon** was a 2006 motion picture based upon *Eragon*, the first book of the *Inheritance* cycle. 
Christopher Paolini originally asked for a small role in the film; according to him, he was supposed to be a Urgal who is beheaded during the Battle under Farthen Dûr by Eragon. The timing of his European book tour created a conflict, however, and he was unable to fulfill his wish.
 
Many fans have expressed anger at both the movie and Stefen Fangmeier for cutting out too many important characters, places, and events. Most fans of the novel have pointed out that nearly none of the characters in the film fit their descriptions at all in the book (except possibly Murtagh and Nasuada - and even Katrina, the Twins, & Elva in the deleted scenes.)
 
A lot have blamed the film for making some changes that will hinder the possible production of a sequel. For example, Roran leaves Palancar Valley just because he doesn't want to be drafted into the army, rather than to earn money to start his own family with Katrina (who did not appear in the film). They also made Brom hunt down and kill the Ra'zac, who are crucial to *Eldest* and the start of *Brisingr*. Also, they made Arya the princess of Ellesméra, but not an elf. Elves inhabiting Ellesméra are crucial to *Eldest*. In addition, Jeod, Helen, Orik, Elva, and Solembum, who did not appear in the movie, feature prominently in the next books. Arguably the largest continuity problem for a sequel is the fact that Eragon's back is not scarred by Durza in the film, which is a major obstacle and plot point through most of *Eldest*. In the movie, they do not mention the other two dragon eggs still in existence (therefore Murtagh cannot become the rider of Thorn), and the Twins (who are important characters in the books) are not seen outside of a deleted scene. As another example, fans criticized the storyline involving the Ra'zac, as they are depicted in the movie as magical beings created by Durza's black magic, yet in the books are actual biological creatures with a thoroughly different appearance than the insect-infested, mummy-like Ra'zac of the film. Others thought the creature effects for Saphira, while very good, made her too slim instead of the muscular build that is freguently mentioned and also gave her an un-draconic look with feathery wings instead of the bat-like ones she and other dragons had in the book. Additionally, Galbatorix's dragon Shruikan (seen at the end scene of the film) is portrayed as being not much larger than Saphira, whereas in the book he is depicted as an enormous dragon with spikes as thick as the trunks of trees.
 
Christopher Paolini has stated that he enjoyed the film. In an open interview[7] held with a fan community in September 2008, he stated that if there were to be another movie, he would be as involved as possible in the process. He further responded that the movie reflected the film-makers' version of the story, whilst the books reflect his version of the story; he was pleased his book was adapted into a movie, given that few books ever are. Furthermore, the movie introduced many new readers to the series. Paolini also stated that "you have to make peace with the nature of the process when signing on the dotted line."
 
This book for the blind is offered in Original or Unified English Braille.What is Original Braille?What is Unified English Braille**Format** Original Contracted Braille - $135.95Unified English Braille - $149.95Original Uncontracted Braille - $183.95Unified Uncontracted Braille - $201.95Shipping: **FREE** From our Production Plant Continue Shopping Product Features Product DescriptionAsk a Question?Product Features- Package Weight: about 14.7 pounds.
Product DescriptionPerfect for fans of Lord of the Rings, the New York Times bestselling Inheritance Cycle about the dragon rider Eragon has sold over 35 million copies and is an international fantasy sensation. Fifteen-year-old Eragon believes that he is merely a poor farm boy - until his destiny as a Dragon Rider is revealed. Gifted with only an ancient sword, a loyal dragon, and sage advice from an old storyteller, Eragon is soon swept into a dangerous tapestry of magic, glory, and power. Now his choices could save - or destroy - the Empire.Ask a Question About this Product(From the Teen Fantasy and Science Fiction shelf.)
 
***Eragon*** is the first book in *The Inheritance Cycle* by American fantasy writer Christopher Paolini. Paolini, born in 1983, began writing the novel after graduating from home school at the age of fifteen.[1] After writing the first draft for a year, Paolini spent a second year rewriting and fleshing out the story and characters. His parents saw the final manuscript and in 2001 decided to self-publish *Eragon;*[2] Paolini spent a year traveling around the United States promoting the novel. The book was discovered by novelist Carl Hiaasen, who brought it to the attention of Alfred A. Knopf. The re-published version was released on August 26, 2003.
 
The book tells the story of a farm boy named Eragon, who finds a mysterious stone in the mountains. The stone is revealed to be a dragon egg, and a dragon he later names Saphira hatches from it. When the evil King Galbatorix finds out about the egg, he sends monstrous servants to acquire it, making Eragon and Saphira flee from their hometown with a storyteller named Brom. Brom, an old member of an extinct group called the Dragon Riders, teaches Eragon about 'The Ways of the Rider.'
 
*Eragon* was the third-best-selling children's hardback book of 2003, and the second-best-selling paperback of 2005. It placed on the *New York Times* Children's Books Best Seller list for 121 weeks and was adapted as a feature film of the same name that was released on December 15, 2006.
 
Christopher Paolini started reading fantasy books when he was 10 years old. At the age of 14, as a hobby, he started writing the first novel in a series of four books, but he could not get beyond a few pages because he had "no idea" where he was going. He began reading everything he could about the "art of writing", and then plotted the whole *Inheritance Cycle* book series. After a month of planning out the series, he started writing the draft of *Eragon* by hand. It was finished a year later, and Paolini began writing the second draft of the book.[3] After another year of editing, Paolini's parents saw the final manuscript. They immediately saw its potential and decided to publish the book through their small, home-based publishing company, Paolini International.[4] Paolini created the cover art for this edition of *Eragon*, which featured Saphira's eye on the cover. He also drew the maps inside the book.[5]
 
The ancient language used by the elves in *Eragon* is based "almost entirely" on Old Norse, German, Anglo Saxon, and Russian myth.[9] Paolini commented: "[I] did a god-awful amount of research into the subject when I was composing it. I found that it gave the world a much richer feel, a much older feel, using these words that had been around for centuries and centuries. I had a lot of fun with that."[10] Picking the right names for the characters and places was a process that could take "days, weeks, or even years". Paolini said: "if I have difficulty choosing the correct moniker, I use a placeholder name until a replacement suggests itself."[4] He added that he was "really lucky" with the name Eragon, "because it's just dragon with one letter changed." Also, Paolini commented that he thought of both parts of the name "Eragon" - "era" and "gone" - as if the name itself changes the era in which the character lives. He thought the name fit the book perfectly, but some of the other names caused him "real headaches".[10]
 
The landscape in *Eragon* is based on the "wild territory" of Paolini's home state, Montana.[3] He said in an interview: "I go hiking a lot, and oftentimes when I'm in the forest or in the mountains, sitting down and seeing some of those little details makes the difference between having an okay description and having a unique description."[10] Paolini also said that Paradise Valley, Montana is "one of the main sources" of his inspiration for the landscape in the book (*Eragon* takes place in the fictional continent Alagaësia). Paolini "roughed out" the main history of the land before he wrote the book, but he did not draw a map of it until it became important to see where Eragon was traveling. He then started to get history and plot ideas from seeing the landscape depicted.[10]
 
Paolini chose to have Eragon mature throughout the book because, "for one thing, it's one of the archetypal fantasy elements". He thought Eragon's growth and maturation throughout the book "sort of mirrored my own growing abilities as a writer and as a person, too. So it was a very personal choice for that book."[10] Eragon's dragon, Saphira, was imagined as "the perfect friend" by Paolini.[3] He decided to go in a more "human direction" with her because she is raised away from her own species, in "close mental contact" with a human. "I considered making the dragon more dragon-like, if you will, in its own society, but I haven't had a chance to explore that. I went with a more human element with Saphira while still trying to get a bit of the magic, the alien, of her race."[10] Paolini made Saphira the "best friend anyone could have: loyal, funny, brave