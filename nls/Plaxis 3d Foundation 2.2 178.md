## Plaxis 3d Foundation 2.2 178

 
 ![Plaxis 3d Foundation 2.2 178](https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcS-dG1ff2_O3cnSdFbDcCHcnjXHvsasZtZl6Epiw6LWS4jqQLFwMYrGMV8)
 
 
**## Files ready for download:
[Link 1](https://tinurll.com/2tz95R)

[Link 2](https://ssurll.com/2tz95R)

[Link 3](https://shoxet.com/2tz95R)

**

 
 
 
 
 
# How to use Plaxis 3D Foundation 2.2 178 for geotechnical analysis
 
Plaxis 3D Foundation is a finite element software package that allows you to perform three-dimensional analysis of deformation and stability in geotechnical engineering. It can handle complex soil-structure interaction problems, such as foundations, excavations, embankments, tunnels, slopes and retaining walls.
 
In this article, we will show you how to use Plaxis 3D Foundation 2.2 178, the latest version of the software, to model and analyze a typical geotechnical problem: a pole-concrete drift cable (PCDC) and pile anchorage system for a long-span bridge. We will also discuss the influence of sedimentation on the structural performance of the PCDC and piles.
 
## Step 1: Create a new project
 
To start a new project in Plaxis 3D Foundation, you need to launch the program and select **New Project** from the **File** menu. You will be asked to enter some basic information about your project, such as name, description, units and coordinate system. You can also choose a template from the **Project Settings** dialog box to set some default parameters for your model.
 
## Step 2: Define the geometry
 
The next step is to define the geometry of your model, which consists of soil layers, structures and boundary conditions. You can use the **Geometry Mode** toolbar to create points, lines, surfaces and volumes. You can also import geometry data from external files or use the **Soil Test** feature to generate soil layers based on borehole data.
 
For this example, we will create a simple geometry that represents a cross-section of a riverbed with two soil layers: clay and sand. We will also create a PCDC structure that consists of a concrete pole, a steel cable and an anchor block. The PCDC is connected to a pile group that consists of four vertical piles and one batter pile. The geometry is shown in Figure 1.
 ![Figure 1: Geometry of the model](figure1.png) 
*Figure 1: Geometry of the model*
 
## Step 3: Assign material properties
 
After defining the geometry, you need to assign material properties to the soil layers and structures. You can use the **Material Database** to select predefined materials or create your own custom materials. You can also use the **Material Models** dialog box to select different constitutive models for the soil behavior, such as linear elastic, Mohr-Coulomb, Hardening Soil or Soft Soil Creep.
 
For this example, we will assign the following material properties:
 
- Clay layer: Hardening Soil model with c = 10 kPa, phi = 20 degrees, E50 = 10 MPa, Eoed = 5 MPa, Eur = 30 MPa, nu = 0.3, m = 0.8 and cref = 0.01 kPa.
- Sand layer: Mohr-Coulomb model with c = 0 kPa, phi = 35 degrees, E = 50 MPa and nu = 0.3.
- Pole: Linear elastic model with E = 30 GPa and nu = 0.2.
- Cable: Linear elastic model with E = 200 GPa and nu = 0.3.
- Anchor block: Linear elastic model with E = 25 GPa and nu = 0.25.
- Piles: Linear elastic model with E = 30 GPa and nu = 0.2.

## Step 4: Define loads and boundary conditions
 
The next step is to define the loads and boundary conditions that act on your model. You can use the **Boreholes Mode**, **Loads Mode** and **B.C.s Mode** toolbars to apply different types of
 842aa4382a
 
 
