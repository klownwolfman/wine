## Legal to Unlock Cell Phones Since November 2006

 
  
 
**## Links to get files:
[Link 1](https://tweeat.com/2tIlOs)

[Link 2](https://urlca.com/2tIlOs)

[Link 3](https://urluso.com/2tIlOs)

**

 
 
 
 
 
by Gary Hunter

Shootings, stabbings, suicides, a death and multiple escapes have turned the Cook County Jail in Chicago on its proverbial ear over the past year, in addition to misconduct by guards, a change in the jails leadership and a lawsuit alleging brutal beatings.

Guns in Jails

On February 1, 2006, prisoners Lorenzo Evans, Terry Martin and Gregory Sherman each sustained bullet wounds from a .32-caliber revolver that had been smuggled into Cook Countys Division 11 maximum security jail.

Randi White, 21, was arrested after investigators unraveled her part in the gun-smuggling scheme. Prosecutor Brian Holmes described a scenario where, on January 26, 2006, White visited Sherman, her boyfriend's brother, at the jail. During that visit White allegedly unscrewed the visiting cage mouthpiece ... and proceeded to hand the revolver to Mr. Sherman.

Sheriff's Dept. spokeswoman Sally Daly offered a different explanation. We believe they unscrewed a plate where the phone was on the wall and passed the contraband through the opening, she said.

White was originally arrested and held on $500,000 bond. On June 2, 2006 she pleaded guilty and was sentenced to 3 years in prison. Evans and Martin likewise accepted guilty pleas and received 3-year sentences for their part in the scheme.

Whites boyfriend, Isiah Sherman, and another man, neither of whom were placed at the scene of the gun smuggling, each received 2-year suspended sentences.

None of the gunshot injuries sustained by the prisoners was serious. Authorities suspect that the three men were plotting to file a lawsuit against the jail after shooting themselves.

In an unrelated incident, a guard was implicated in smuggling a weapon into the jail. Former Cook County guard Kenyatta Sanders was sentenced to 15 years in prison on February 21, 2006. She was convicted on a variety of charges including attempting to smuggle a loaded revolver and cell phone to prisoner Daniel Jimerson.

Sanders never admitted to a relationship between herself and Jimerson or to smuggling the gun, but an investigation turned up letters she had written to her incarcerated lover.

I want you inside and out, but I need your help ... I have never been in love with a man in the system, wrote Sanders. Another excerpt stated, Nobody can love you like me because [our] love is not a result of sex, money or material things, but is from two people connecting from the soul. We are soulmates until death do us part.

On January 20, 2003, Sanders had wriggled through a 9-inch opening to avoid a metal detector, but was confronted by other jail staff when she tried to gain unauthorized entry to a tier where Jimerson was housed. A search turned up the gun, a cell phone and a knife with a 4-inch serrated blade.

Earlier this year a gun was smuggled into another Illinois county jail, too, although by different means. Victoria Lundy managed to smuggle a .25 caliber pistol into the Ross County Jail on January 29, 2006. The gun was discovered after it accidentally discharged inside a holding cell.

Lundy was originally questioned about an incident of shots being fired, then was arrested for driving without a license. Another woman in the cell with Lundy said Lundy had gone to the bathroom several times before the gun went off.

A close inspection of the weapon revealed an off white colored liquid substance on the gun with red specks [that] appeared to be consistent with vaginal fluid.

Lundy later admitted to the original shooting. Her charges include improperly discharging a weapon, illegal conveyance of a weapon and carrying a [very well] concealed weapon.

Escapes and security failures

Cook County jail prisoner Randy Rencher escaped from the facility on June 27, 2005. Rencher faced a variety of charges and is suspected of at least two bank robberies that occurred after his escape. Both banks were robbed by a man wearing a jail guards uniform. Of major concern was a statement allegedly made by Rencher that he had bribed a guard to help him escape. Rencher, who later turned himself in on November 2, 2005, was the first prisoner to escape from Cook County Jail in a decade. But he wasn't the last.

The Division 11 maximum security jail was embarrassed again on February 10, 2006 when Warren C. Mathis made a clean getaway in a truck carrying dirty laundry.

At some point, it is believed Mathis slipped away from the officer who was supervising the work detail and hid in a truck that was picking up inmate laundry, said Sheriff Michael F. Sheahans press office. Mathis was facing three armed robbery charges when he arranged his unauthorized early release. He was captured a day later.

On February 11, 2006, six prisoners overpowered a guard in their escape from the Cook County jail, Division 10.

The well-planned ruse was based on intimate knowledge of jail routines and security weaknesses. Using homemade knives, Eric Bernard, David Earnest, Tyrone Everhart, Arnold Joyner, Michael McIntosh and Francisco Romero subdued guard Darin Gater after a seventh prisoner, Patrell Doss, first blinded Gater with a hot-water concoction.

We had planned and plotted it for more than seven to eight months, Earnest told an NBC5 reporter after he was captured. It was disinfectant and soap in his eyes. By the time he could see, when he opened his eyes, he had about three knives and two inmates standing in front of him.

The escape began when Gater arrived on duty alone. Staffing shortages resulted in only Gater being assigned to what is normally a two-guard position. At some point Gater unlocked Doss and escorted him to the showers. Thats when Doss dashed Gater with the soapy water, blinding him long enough for the other prisoners to arrive.

Gater was handcuffed and Doss donned the guard's uniform and took his keys. Using a fire as a diversion, Doss was able to secure the other prisoners' escape but was himself trapped in the process. The remaining six managed to get outside the building, where they scaled a barbed-wire fence and ran away.

Joyner, Earnest and Everhart were captured the next day without incident. Bernard, McIntosh and Romero were caught on Feb. 13 after a home invasion and a standoff with over three dozen police.

Outside accomplices Michelle Reyes, Jose Romero and Anna Romero were charged with aiding in the escape of McIntosh and Romero after they spent the day driving the fugitives around the city. Each was given a $200,000 bond.

The case took a bizarre twist when Gater admitted under interrogation that he was part of the escape plot. Prosecutors say the guard confessed to giving the prisoners his keys and uniform and then handcuffing himself. Gaters alleged motive was to embarrass outgoing Sheriff Michael Sheahan and to help his former supervisor, Richard Remus, a candidate for Sheriff, get elected.

Gater did not deny making the comments but his attorney, Tommy Brewer, said his client was coerced. Anything to stop the questioning, said Brewer. They denied him sleep, [he was] not allowed to eat, they wouldnt let him make phone calls. Brewer claimed that Gater was being made into a fall guy.

Sheriff Sheahan disagreed, stating, It's [Gater's] words that hes talked about the political motivation and he talked about what went down and he admitted everything that he did. Gater was charged on February 14, 2006 and posted a $500,000 bond.

Other security lapses were discovered in connection with the escape. Cook County jail Sgt. Ivan Hernandez was told by his supervisor, Capt. Michael Wright, that a prisoner had a knife and was planning to escape but no cell search was ever conducted.

That tip came at about 3:30 p.m. on the day of the escape. Hernandez and Wright consistently tried to shift the blame to each other. Hernandez said he was waiting on Wright to call back and authorize the cell search. Wright stated that no authorization was required and that Hernandez should have searched the cell on his own authority.

Jail monitor Charles Fasano faulted both men. He noted that Sgt. Hernandez should have performed the search. But he also said of Capt. Wright, You would want to call back and say what did you find? Did you find a knife?

Hernandez and five other guards were eventually suspended. Four of those guards have alleged that they also were held and questioned for over 24 hours without an attorney. All of the guards and supervisors agreed that the jail was short-staffed that night. One guard even admitted to being told about a knife but insisted that no one had said anything about an escape plot.

When asked if any guards helped them escape, Earnest said, No. Not at all. Everything we needed was there. We had seen it. It was like a walk in the park, he added.

One week after the mass escape from the jail, on Friday, February 17, 2006, Cook County prisoner Lawanda Warren, 40, walked out of a hospital where she was being treated after the guard watching her went to answer a phone.

Warren was being held on drug charges. Police tracked her down through her family members, who eventually convinced her to surrender. Cook County Sheriff Sheahan took Warren back into custody himself. 

Beatings, Deaths and Suicides

Cook County jail guards are currently locked in a court battle against prisoners over an earlier riot that broke out in the jail in July 2000; also, two former guards are pursuing a separate lawsuit claiming they faced retaliation and were forced to resign after refusing to cover-up what happened during the 2000 incident.

Nathson Fields described how he and other prisoners were attacked by out-of-control guards who were intent on making a point during the 2000 beatings. Nathson said of one of the Cook Cou