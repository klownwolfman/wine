## Xentrix Discography 19871996

 
 ![Xentrix Discography 19871996](https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRsvRMtIlKRq0MM5sLjFJ8NZI05B0eoluXAzeXVhkBs-dx_BJIOsSf0yBg)
 
 
**## Links to get files:
[Link 1](https://jinyurl.com/2tzC6v)

[Link 2](https://tweeat.com/2tzC6v)

[Link 3](https://urlca.com/2tzC6v)

**

 
 
 
 
 
# Xentrix Discography 19871996: A Review of the English Thrash Metal Band's Albums
  
Xentrix are an English thrash metal band from Preston, Lancashire, who formed in 1984 under the name Sweet Vengeance.[^1^] They changed their name to Xentrix in 1988 and released four albums between 1989 and 1996: *Shattered Existence*, *For Whose Advantage?*, *Kin* and *Scourge*.[^2^] In this article, we will review each of these albums and explore how Xentrix evolved their sound and style over the years.
  
## Shattered Existence (1989)
  
The debut album of Xentrix was released in 1989 by Roadrunner Records, after the band attracted attention with a five-star rating from Kerrang! magazine.[^1^] The album features ten tracks of fast and aggressive thrash metal, influenced by bands like Metallica, Slayer and Exodus. The songs deal with topics such as war, religion, corruption and violence, with lyrics that are often dark and cynical. The album showcases the band's technical skills and tight performance, especially on tracks like \"No Compromise\", \"Balance of Power\" and \"Dark Enemy\". The album also includes a cover of Ray Parker Jr.'s \"Ghostbusters\" theme, which caused some controversy due to the unauthorized use of the Ghostbusters logo on the original artwork.[^1^]
  
## For Whose Advantage? (1990)
  
The second album of Xentrix was released in 1990 by Roadrunner Records, and is considered by many fans and critics as their best work. The album features eleven tracks of refined and mature thrash metal, with more melodic and progressive elements than their previous album. The songs are more varied and complex, with changes in tempo, mood and structure. The lyrics are also more thoughtful and socially conscious, addressing issues such as environmentalism, consumerism, media manipulation and human rights. The album contains some of the band's most memorable songs, such as \"Questions\", \"For Whose Advantage?\", \"The Human Condition\" and \"Desperate Remedies\". The album also includes a cover of Rush's \"Hemispheres\", which showcases the band's musical versatility.
  
## Kin (1992)
  
The third album of Xentrix was released in 1992 by Roadrunner Records, and marked a significant change in their musical direction. The album features nine tracks of more experimental and diverse thrash metal, with influences from alternative rock, industrial music and funk. The songs are more melodic and atmospheric, with less emphasis on speed and aggression. The lyrics are also more personal and introspective, exploring themes such as identity, relationships, emotions and dreams. The album received mixed reviews from fans and critics, who praised the band's creativity but also criticized their departure from their original style. Some of the highlights of the album are \"The Order of Chaos\", \"New Beginnings\", \"See Through You\" and \"Bad Blood\".
  
## Scourge (1996)
  
The fourth and final album of Xentrix was released in 1996 by Heavy Metal Records, after a four-year hiatus. The album features ten tracks of heavy and groovy thrash metal, with influences from Pantera, Machine Head and Sepultura. The songs are more straightforward and catchy, with more focus on riffs and hooks. The lyrics are also more aggressive and defiant, reflecting the band's frustration with the music industry and their personal issues. The album received positive reviews from fans and critics, who welcomed the band's return to form. Some of the standout tracks are \"13 Years\", \"The Hand That Feeds Itself\", \"Incite\" and \"Pure Thought\".
  
### Conclusion
  
Xentrix Discography 19871996 is a testament to the band's talent and evolution as one of England's most successful thrash metal bands. From their debut album *Shattered Existence*, which established them as a force to be reckoned with in the underground metal scene, to their final album *Scourge*, which showed them
 842aa4382a
 
 
