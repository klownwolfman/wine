## Cisco 7920 Configuration Utility

 
  
 
**Click Here ►►► [https://urllio.com/2tCDXh](https://urllio.com/2tCDXh)**

 
 
 
 
 
# How to Install and Use the Cisco 7920 Configuration Utility
 
The Cisco 7920 Configuration Utility is a software tool that allows you to configure network and phone settings for the Cisco Wireless IP Phone 7920. This wireless IP phone is an easy-to-use device that provides comprehensive voice communications in conjunction with Cisco Unified CallManager and Cisco Unified CallManager Express. In this article, we will show you how to install and use the Cisco 7920 Configuration Utility to customize your phone features and preferences.
 
## What You Need to Install the Cisco 7920 Configuration Utility
 
To install and run the Cisco 7920 Configuration Utility, you need the following hardware and software requirements:
 
- An IBM-compatible personal computer (PC) that runs Microsoft Windows
- A CD-ROM drive or an internet connection to download the software
- A keyboard and mouse or other pointing device
- A USB port and cable to connect the PC to the phone
- One of these operating systems on the PC: Windows 98 (Second Edition), Windows ME, Windows 2000 Professional (SP1 or later), or Windows XP (Home Edition or Professional)
- The Cisco 7920 Configuration Utility Version 2.0
- The Cisco Wireless IP Phone 7920 Firmware Release 2.0

Note: The Cisco 7920 Configuration Utility 2.0 supports only Cisco Wireless IP Phone 7920 Firmware Release 2.0.
 
## How to Install the Cisco 7920 Configuration Utility
 
To install the Cisco 7920 Configuration Utility, follow these steps:

1. Insert the Cisco 7920 Configuration Utility CD into the CD-ROM drive or download the software from [Cisco Software Download Center](https://software.cisco.com/download/portal/select.html?&mdfid=278040707&softwareid=282074239).
2. Double-click the setup.exe file. The InstallShield Wizard window appears.
3. The InstallShield Wizard provides step-by-step instructions that guide you through the installation process. Click Next to continue, and follow the on-screen instructions.
4. When prompted, choose a destination folder for the program files. The default folder is C:\Program Files\Cisco Systems\Cisco Wireless IP Phone 7920.
5. When prompted, choose a folder for the program shortcuts. The default folder is Start Menu\Programs\Cisco Systems\Cisco Wireless IP Phone 7920.
6. When prompted, choose whether to create a desktop icon for the program.
7. When prompted, choose whether to launch the program after installation.
8. Click Finish to complete the installation.

## How to Uninstall the Cisco 7920 Configuration Utility
 
If you want to uninstall the Cisco 7920 Configuration Utility, follow these steps:

1. Click Start > Settings > Control Panel > Add/Remove Programs.
2. Select Cisco Wireless IP Phone 7920 from the list of programs and click Change/Remove.
3. The InstallShield Wizard window appears. Click Next to continue, and follow the on-screen instructions.
4. Click Finish to complete the uninstallation.

## How to Use the Cisco 7920 Configuration Utility
 
To use the Cisco 7920 Configuration Utility, you need to connect your PC to your phone using a USB cable. You can use either the proprietary USB cable that ships with the Configuration Utility software or a standard type 2 USB cable with a desktop charger. To connect your PC to your phone, follow these steps:

1. Activate the USB port on your phone by pressing Menu > Settings > Network Profiles > USB Port > On.
2. Connect one end of the USB cable to your PC and the other end to your phone or desktop charger.
3. If you are using a desktop charger, place your phone on the charger cradle.
4. The phone displays \"USB Connected\" on its screen.
5. Launch the Cisco 7920 Configuration Utility on your PC by clicking Start > Programs > Cisco Systems > Cisco Wireless IP Phone 7920 > Cisco Wireless IP Phone 842aa4382a




