## HTML5 specification published

 
 ![HTML5 Specification Published](https://idge.staticworld.net/ifw/IFW_logo_social_300x300.png)
 
 
**## File links for downloading:
[Link 1](https://cinurl.com/2tIAqZ)

[Link 2](https://byltly.com/2tIAqZ)

[Link 3](https://urllio.com/2tIAqZ)

**

 
 
 
 
 
**HTML5** is a markup language used for structuring and presenting content on the World Wide Web. It is the fifth and final[3] major HTML version that is a World Wide Web Consortium (W3C) recommendation. The current specification is known as the HTML Living Standard. It is maintained by the Web Hypertext Application Technology Working Group (WHATWG), a consortium of the major browser vendors (Apple, Google, Mozilla, and Microsoft).
 
Many new syntactic features are included. To natively include and handle multimedia and graphical content, the new , and elements were added, expandable sections are natively implemented through ... and ... rather than depending on CSS or JavaScript, and support for scalable vector graphics (SVG) content and MathML for mathematical formulas was also added. To enrich the semantic content of documents, new page structure elements such as , , , , , , , and are added. New attributes were introduced, some elements and attributes were removed, and others such as , , and were changed, redefined, or standardized. The APIs and Document Object Model (DOM) are now fundamental parts of the HTML5 specification,[7] and HTML5 also better defines the processing for any invalid documents.[8]
 
On 14 February 2011, the W3C extended the charter of its HTML Working Group with clear milestones for HTML5. In May 2011, the working group advanced HTML5 to "Last Call", an invitation to communities inside and outside W3C to confirm the technical soundness of the specification. The W3C developed a comprehensive test suite to achieve broad interoperability for the full specification by 2014, which was the target date for recommendation.[25] In January 2011, the WHATWG renamed its "HTML5" specification *HTML Living Standard*. The W3C nevertheless continued its project to release HTML5.[26]
 
In July 2012, WHATWG and W3C decided on a degree of separation. W3C will continue the HTML5 specification work, focusing on a single definitive standard, which is considered a "snapshot" by WHATWG. The WHATWG organization continues its work with HTML5 as a "living standard". The concept of a living standard is that it is never complete and is always being updated and improved. New features can be added but functionality will not be removed.[27]
 
On 16 September 2014, W3C moved HTML5 to Proposed Recommendation.[30] On 28 October 2014, HTML5 was released as a W3C Recommendation,[31] bringing the specification process to completion.[4] On 1 November 2016, HTML5.1 was released as a W3C Recommendation.[32] On 14 December 2017, HTML5.2 was released as a W3C Recommendation.[33]
 
In at least one case, namely the permissible content of the element, the two specifications directly contradicted each other (as of July 2018),[update] with the W3C definition allowing a broader range of uses than the WHATWG definition.[53][54]
 
The "Introduction" section in the WHATWG spec (edited by Ian "Hixie" Hickson) is critical of W3C, e.g. "Note: Although we have asked them to stop doing so, the W3C also republishes some parts of this specification as separate documents." In its "History" subsection it portrays W3C as resistant to Hickson's and WHATWG's original HTML5 plans, then jumping on the bandwagon belatedly (though Hickson was in control of the W3C HTML5 spec, too). Regardless, it indicates a major philosophical divide between the organizations:[55]
 
For a number of years, both groups then worked together. In 2011, however, the groups came to the conclusion that they had different goals: the W3C wanted to publish a "finished" version of "HTML5", while the WHATWG wanted to continue working on a Living Standard for HTML, continuously maintaining the specification rather than freezing it in a state with known problems, and adding new features as needed to evolve the platform.
 
The W3C proposed a greater reliance on modularity as a key part of the plan to make faster progress, meaning identifying specific features, either proposed or already existing in the spec, and advancing them as separate specifications. Some technologies that were originally defined in HTML5 itself are now defined in separate specifications:
 
Some features that were removed from the original HTML5 specification have been standardized separately as modules, such as Microdata and Canvas. Technical specifications introduced as HTML5 extensions such as Polyglot markup have also been standardized as modules. Some W3C specifications that were originally separate specifications have been adapted as HTML5 extensions or features, such as SVG. Some features that might have slowed down the standardization of HTML5 were or will be standardized as upcoming specifications, instead.
 
The HTML5 syntax is no longer based on SGML[96][97] despite the similarity of its markup. It has, however, been designed to be backward-compatible with common parsing of older versions of HTML. It comes with a new introductory line that looks like an SGML document type declaration, , which triggers the standards-compliant rendering mode.[98]Since 5 January 2009, HTML5 also includes *Web Forms 2.0*, a previously separate WHATWG specification.[99][100]
 
Not all of the above technologies are included in the W3C HTML5 specification, though they are in the WHATWG HTML specification.[114] Some related technologies, which are not part of either the W3C HTML5 or the WHATWG HTML specification, are as follows. The W3C publishes specifications for these separately:
 
XML documents must be served with an XML Internet media type (often called "MIME type") such as application/xhtml+xml or application/xml,[102] and must conform to strict, well-formed syntax of XML. XHTML5 is simply XML-serialized HTML5 data (that is, HTML5 constrained to XHTML's strict requirements, e.g., not having any unclosed tags), sent with one of XML media types. HTML that has been written to conform to both the HTML and XHTML specifications and therefore produces the same DOM tree whether parsed as HTML or XML is known as polyglot markup.[126]
 
HTML5 is designed so that old browsers can safely ignore new HTML5 constructs.[7] In contrast to HTML 4.01, the HTML5 specification gives detailed rules for lexing and parsing, with the intent that compliant browsers will produce the same results when parsing incorrect syntax.[128] Although HTML5 now defines a consistent behavior for "tag soup" documents, those documents do not conform to the HTML5 standard.[128]
 
This specification defines the 5th major revision of the core language of the World Wide Web: the Hypertext Markup Language (HTML). In this version, new features are introduced to help Web application authors, new elements are introduced based on research into prevailing authoring practices, and special attention has been given to defining clear conformance criteria for user agents in an effort to improve interoperability.
 
Work on evolutions of this specification proceeds at The HTML5 Recommendation represents a milestone in the development of HTML but far from being the end of the road and improvements are already well under way. It is possible that future versions will no longer be published as a monolithic specification but rather as a set of smaller modules. Irrespective of whether that is the case or not, will be maintained as the entry point to the entirety of HTML technology.
 
This document has been reviewed by W3C Members, by software developers, and by other W3C groups and interested parties, and is endorsed by the Director as a W3C Recommendation. It is a stable document and may be used as reference material or cited from another document. W3C's role in making the Recommendation is to draw attention to the specification and to promote its widespread deployment. This enhances the functionality and interoperability of the Web.
 
Work on this specification is also done at the WHATWG. The W3C HTML working group actively pursues convergence of the HTML specification with the WHATWG living standard, within the bounds of the W3C HTML working group charter. There are various ways to follow this work at the WHATWG:
 
This section describes the status of this document at the time of its publication. Other documents may supersede this document. A list of current W3C publications and the most recently formally published revision of this technical report can be found in the W3C technical reports index at
 
The working groups maintains a list of all bug reports that the editor has not yet tried to address and a list of issues for which the chairs have not yet declared a decision. The editor also maintains a list of all e-mails that he has not yet tried to address. These bugs, issues, and e-mails apply to multiple HTML-related specifications, not just this one.
 
Implementors should be aware that this specification is not stable. **Implementors who are not taking part in the discussions are likely to find the specification changing out from under them in incompatible ways.** Vendors interested in implementing this specification before it eventually reaches the Candidate Recommendation stage should join the aforementioned mailing lists and take part in the discussions.
 
The publication of this document by the W3C as a W3C Working Draft does not imply that all of the participants in the W3C HTML working group endorse the contents of the specification. Indeed, for any section of the specification, one can usually find many members of the working group or of the W3C as a whole who object strongly to the current text, the existence of the section at all, or the idea that the working group should even spend time discussing the concept of that s