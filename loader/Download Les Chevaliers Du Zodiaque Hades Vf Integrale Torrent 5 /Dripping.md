## Dripping

 
 ![Dripping](https://www.textures4photoshop.com/tex/thumbs/realistic-dripping-blood-png-with-transparent-background-228.png)
 
 
**## Links to download files:
[Link 1](https://geags.com/2tEtd2)

[Link 2](https://urllie.com/2tEtd2)

[Link 3](https://tiurll.com/2tEtd2)

**

 
 
 
 
 
**Dripping**, also known usually as **beef dripping** or, more rarely, as **pork dripping**, is an animal fat produced from the fatty or otherwise unusable parts of cow or pig carcasses. It is similar to lard, tallow and schmaltz.
 
It is used for cooking, especially in British cuisine, significantly so in the Midlands and Northern England, though towards the end of the 20th century dripping fell out of favour due to it being regarded as less healthy than vegetable oils such as olive or sunflower.
 
Preparation is traditionally described as collection of the residue from meat roasts but modern production is from such residue added to boiling water with a generous amount of salt (about 2g per litre). When the stock pot is chilled a solid lump of dripping (the cake) settles. The stock pot should be scraped clean and re-chilled for future use. The residue can be reprocessed for more dripping and strained through a cheesecloth lined sieve as an ingredient for a beef stock. Dripping can be clarified by adding a sliced raw potato and cooking until potato turns brown. The cake will be the colour and texture of ghee.
 
Pork or beef dripping can be served cold, spread on bread and sprinkled with salt and pepper (*bread and dripping*). If the flavourful brown sediment and stock from the roast has settled to the bottom of the dripping and coloured it brown, then in parts of Yorkshire it is known colloquially as a "mucky fat" sandwich.
 72ea393242
 
 
