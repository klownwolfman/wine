## writing essay conclusions

 
 ![Writing Essay Conclusions](https://www.scribbr.com/wp-content/uploads/2021/02/open-graph-scribbrcom.png)
 
 
**## File links for downloading:
[Link 1](https://cinurl.com/2tIlNW)

[Link 2](https://byltly.com/2tIlNW)

[Link 3](https://urllio.com/2tIlNW)

**

 
 
 
 
 
Avoid simply summarizing each paragraph or repeating each point in order; try to bring your points together in a way that makes the connections between them clear. The conclusion is your final chance to show how all the paragraphs of your essay add up to a coherent whole.
 
Even if your essay has explored different points of view, your own position should be clear. There may be many possible approaches to the topic, but you want to leave the reader convinced that yours is the best one!
 
This conclusion is taken from a short expository essay that explains the invention of the printing press and its effects on European society. It focuses on giving a clear, concise overview of what was covered in the essay.
 
So much is at stake in writing a conclusion. This is, after all, your last chance to persuade your readers to your point of view, to impress yourself upon them as a writer and thinker. And the impression you create in your conclusion will shape the impression that stays with your readers after they've finished the essay.
 
The end of an essay should therefore convey a sense of completeness and closure as well as a sense of the lingering possibilities of the topic, its larger meaning, its implications: the final paragraph should close the discussion without closing it off.
 
Explain how these parts are connected. For example, in the animal-shelter essay, you might point out that adopting a shelter dog helps more animals because your adoption fee supports the shelter, which makes your choice more socially responsible.
 
One of the most important functions of the conclusion is to provide context for your argument. Your reader may finish your essay without a problem and understand your argument without understanding why that argument is important. Your introduction might point out the reason your topic matters, but your conclusion should also tackle this questions. Here are some strategies for making your reader see why the topic is important:
 
The first steps for writing any college essay are coming up with a strong thesis statement and composing a rough introduction. Once you've done that, you can collect information that supports your thesis, outline your essay's main points, and start writing your body paragraphs. Before you can submit the essay, though, you'll also need to write a compelling conclusion paragraph.
 
The conclusion's primary role is to convince the reader that your argument is valid. Whereas the introduction paragraph says, "Here's what I'll prove and how," the conclusion paragraph says, "Here's what I proved and how." In that sense, these two paragraphs should closely mirror each other, with the conclusion restating the thesis introduced at the beginning of the essay.
 
The new insight you raise in your conclusion should ideally come from the research you already conducted. Should a new idea come to you while writing the body paragraphs, go ahead and make a note to remind you to allude to it in your conclusion.
 
Formal essay-writing typically avoids first- and second-person pronouns such as "I" and "you." There are, however, two exceptions to this rule, and these are the introduction and conclusion paragraphs.
 
In the introduction, you may use the words "I" or "me" just once to clarify that the essay's claim is your own. In the conclusion, you may use first-person pronouns to attempt to establish an emotional connection with the reader, as long as this connection is related in some way to the overarching claim.
 
Laila Abdalla, Ph.D., is a career coach and advocate for individuals on temporary state assistance. She taught college and graduate courses in English and writing for 20+ years. Abdalla devotes her teaching, leadership, and career to equity, diversity, and inclusion.\r\n\r\nAbove all, she is committed to her clients' and students' complete experience, raising awareness of BIPOC issues in employment, language, community, and culture. She leads with equity in management and nonprofit volunteering and continues to develop her own understandings of these complex issues \u2014 both professionally and in her lived experiences.\r\n\r\nAbdalla has her Ph.D. in English from McGill University in Montreal, Canada.\r\n\r\nAbdalla is a paid member of the Red Ventures Education Integrity Network.","image":"https:\/\/res.cloudinary.com\/highereducation\/images\/c\_fill,g\_face,f\_auto,q\_auto,h\_60,w\_60\/v1659637724\/BestColleges.com\/laila-abdalla\/laila-abdalla.png?\_i=AA","link":"https:\/\/www.bestcolleges.com\/contributors\/laila-abdalla\/","linkedin":"","twitter":"","web":"","career":"","subject":"Anti-bias ","categories":["name":"Reviewer","slug":"reviewer","name":"Writer","slug":"writer"],"interviews":[],"events":[]}],"date":"November 4, 2020","content":"The thesis is central to an argumentative essay. These strategies and thesis statement examples will teach you how to write a quality essay introduction.","id":629},"link":"https:\/\/www.bestcolleges.com\/blog\/essay-introduction\/","image":"https:\/\/res.cloudinary.com\/highereducation\/image\/upload\/f\_auto,fl\_lossy,q\_auto\/v1604426294\/BestColleges.com\/Blog\/BC-Blog\_Collge-Essay-Introduction\_11.4.20\_FTR.jpg","title":"How to Write an Essay Introduction","author":["id":12104,"name":"Laila Abdalla, Ph.D.","description":"Laila Abdalla, Ph.D., is a career coach and advocate for individuals on temporary state assistance. She taught college and graduate courses in English and writing for 20+ years. Abdalla devotes her teaching, leadership, and career to equity, diversity, and inclusion.\r\n\r\nAbove all, she is committed to her clients' and students' complete experience, raising awareness of BIPOC issues in employment, language, community, and culture. She leads with equity in management and nonprofit volunteering and continues to develop her own understandings of these complex issues \u2014 both professionally and in her lived experiences.\r\n\r\nAbdalla has her Ph.D. in English from McGill University in Montreal, Canada.\r\n\r\nAbdalla is a paid member of the Red Ventures Education Integrity Network.","image":"https:\/\/res.cloudinary.com\/highereducation\/images\/c\_fill,g\_face,f\_auto,q\_auto,h\_60,w\_60\/v1659637724\/BestColleges.com\/laila-abdalla\/laila-abdalla.png?\_i=AA","link":"https:\/\/www.bestcolleges.com\/contributors\/laila-abdalla\/","linkedin":"","twitter":"","web":"","career":"","subject":"Anti-bias ","categories":["name":"Reviewer","slug":"reviewer","name":"Writer","slug":"writer"],"interviews":[],"events":[]],"date":"November 21, 2022","content":"College students write many papers, but what's the best way to compose an essay introduction? Learn how to come up with an interesting hook and thesis.","id":554,"link":"https:\/\/www.bestcolleges.com\/blog\/how-to-write-a-body-paragraph\/","image":"https:\/\/res.cloudinary.com\/highereducation\/image\/upload\/f\_auto,fl\_lossy,q\_auto\/v1613603198\/BestColleges.com\/Blog\/BC-Blog\_HowToWriteBodyParagraph\_2.18.2021\_FTR.jpg","title":"How to Write a Body Paragraph for a College Essay","author":["id":12143,"name":"Staff Writers","description":"The staff writers for BestColleges collaborate to deliver unique, student-driven content on topics such as career development, college life, and college planning.","image":"https:\/\/res.cloudinary.com\/highereducation\/images\/c\_fill,g\_face,f\_auto,q\_auto,h\_60,w\_60\/v1668538345\/BestColleges.com\/logo-circle\_55998d056d\/logo-circle\_55998d056d.png?\_i=AA","link":"https:\/\/www.bestcolleges.com\/contributors\/staff-writers\/","linkedin":"","twitter":"","web":"","career":"","subject":"","categories":["name":"Writer","slug":"writer"],"interviews":[],"events":[]],"date":"November 22, 2022","content":"Part of doing well in college is learning how to write a body paragraph, which should support your essay's thesis claim through evidence and cogent analysis.","id":550]; Explore More College Resources View all Strategies for Writing a Compelling Thesis Statement by Laila Abdalla, Ph.D. November 4, 2020 How to Write an Essay Introduction by Laila Abdalla, Ph.D. November 4, 2020 How to Write a Body Paragraph for a College Essay by Staff Writers February 18, 2021 BestColleges.com is an advertising-supported site. Featured or trusted partner programs and all school search, finder, or match results are for schools that compensate us. This compensation does not influence our school rankings, resource guides, or other editorially-independent information published on this site.
 
Finally, **draw together the question**, the evidence in the essay body, and the conclusion. This way the reader knows that you have understood and answered the question. This part needs to be clear and concise.
 
At the very end of the essay comes your closing sentence or clincher. As you think about how to write a good conclusion, the clincher must be top of mind. What can you say to propel the reader to a new view on the subject? This final sentence needs to help readers feel a sense of closure. It should also end on a positive note, so your audience feels glad they read your paper and that they learned something worthwhile.
 
The conclusion of an essay may be the toughest section to write. Think about it; you're really tired at this point. It's probably the night before your paper is due and you just want to be done. So, the temptation is there to simply rush through it, and hope that your teacher is exhausted once she gets to your paper and doesn't bother to read it fully.
 
Remember that thesis statement which you wrote in the first or second paragraph of your essay? You know, the one where you stated a claim