## Lesbian Chronicles Chapter 6

 
  
 
**Lesbian Chronicles Chapter 6 »»» [https://urllio.com/2tEdYj](https://urllio.com/2tEdYj)**

 
 
 
 
 
The survey finds that 12 is the median age at which lesbian, gay and bisexual adults first felt they might be something other than heterosexual or straight. For those who say they now know for sure that they are lesbian, gay, bisexual or transgender, that realization came at a median age of 17.
 
This report is based primarily on a Pew Research Center survey of the LGBT population conducted April 11-29, 2013, among a nationally representative sample of 1,197 self-identified lesbian, gay, bisexual and transgender adults 18 years of age or older. The sample comprised 398 gay men, 277 lesbians, 479 bisexuals and 43 transgender adults. The survey questionnaire was written by the Pew Research Center and administered by the GfK Group using KnowledgePanel, its nationally representative online research panel.
 
On the topic of same-sex marriage, not surprisingly, there is a large gap between the views of the general public and those of LGBT adults. Even though a record 51% of the public now favors allowing gays and lesbians to marry legally, up from 32% in 2003, that share is still far below the 93% of LGBT adults who favor same-sex marriage.
 
Four-in-ten respondents to the Pew Research Center survey identify themselves as bisexual. Gay men are 36% of the sample, followed by lesbians (19%) and transgender adults (5%).2 While these shares are consistent with findings from other surveys of the LGBT population, they should be treated with caution.3 There are many challenges in estimating the size and composition of the LGBT population, starting with the question of whether to use a definition based solely on self-identification (the approach taken in this report) or whether to also include measures of sexual attraction and sexual behavior.
 
The survey also finds that bisexuals differ from gay men and lesbians on a range of attitudes and experiences related to their sexual orientation. For example, while 77% of gay men and 71% of lesbians say most or all of the important people in their lives know of their sexual orientation, just 28% of bisexuals say the same. Bisexual women are more likely to say this than bisexual men (33% vs. 12%). Likewise, about half of gay men and lesbians say their sexual orientation is extremely or very important to their overall identity, compared with just two-in-ten bisexual men and women.
 
Gays and lesbians are also more likely than bisexuals to say their sexual orientation is a positive factor in their lives, though across all three subgroups, many say it is neither positive nor negative. Only a small fraction of all groups describe their sexual orientation or gender identity as a negative factor.
 
Roughly three-quarters of bisexual respondents to the Pew Research survey are women. By contrast, gay men outnumber lesbians by about two-to-one among survey respondents. Bisexuals are far more likely than either gay men or lesbians to be married, in part because a large majority of those in committed relationships have partners of the opposite sex and thus are able to marry legally. Also, two-thirds of bisexuals say they either already have or want children, compared with about half of lesbians and three-in-ten gay men.
 
Across the LGBT population, more say bisexual women and lesbians are accepted by society than say this about gay men, bisexual men or transgender people. One-in-four respondents say there is a lot of social acceptance of lesbians, while just 15% say the same about gay men. Similarly, there is more perceived acceptance of bisexual women (33% a lot) than of bisexual men (8%). Transgender adults are viewed as less accepted by society than other LGBT groups: only 3% of survey respondents say there is a lot of acceptance of this group.
 
For example, younger gay men and lesbians are more likely to have disclosed their sexual orientation somewhat earlier in life than have their older counterparts. Some of this difference may be attributable to changing social norms, but some is attributable to the fact that the experiences of young adults who have not yet identified as being gay or lesbian but will do so later in life cannot be captured in this survey.
 
As for gender patterns, the survey finds that lesbians are more likely than gay men to be in a committed relationship (66% versus 40%); likewise, bisexual women are much more likely than bisexual men to be in one of these relationships (68% versus 40%). In addition women, whether lesbian or bisexual, are significantly more likely than men to either already have children or to say they want to have children one day.
 
When asked in an open-ended question to name the national public figures most responsible for advancing LGBT rights, President Barack Obama, who announced last year that he had changed his mind and supports gay marriage, tops the list along with comedian and talk show host Ellen DeGeneres, who came out as a lesbian in 1997 and has been a leading advocate for the LGBT population ever since then. Some 23% of respondents named Obama and 18% named DeGeneres. No one else was named by more than 3% of survey respondents.
 
For the most part LGBT adults are in broad agreement on which institutions they consider friendly to people who are lesbian, gay, bisexual and transgender. Seven-in-ten describe the entertainment industry as friendly, 63% say the same about the Obama administration, and 57% view the Democratic Party as friendly. By contrast, just 4% say the same about the Republican Party (compared with 76% who say it is unfriendly); 8% about the military (47% unfriendly) and 4% about professional sports leagues (59% unfriendly). LGBT adults have mixed views about the news media, with 27% saying it is friendly, 56% neutral and 16% unfriendly.
 
When it comes to community engagement, gay men and lesbians are more involved than bisexuals in a variety of LGBT-specific activities, such as attending a gay pride event or being a member of an LGBT organization.
 
There are big differences across LGBT groups in how they use social networking sites. Among all LGBT adults, 55% say they have met new LGBT friends online or through a social networking site. Gay men are the most likely to say they have done this (69%). By contrast, about half of lesbians (47%) and bisexuals (49%) say they have met a new LGBT friend online.
 
About four-in-ten LGBT adults (43%) have revealed their sexual orientation or gender identity on a social networking site. While roughly half of gay men and lesbians have come out on a social network, only about one-third (34%) of bisexuals say they have done this.
 
People who are transgender may also describe themselves as heterosexual, gay, lesbian, or bisexual. In the Pew Research Center survey, respondents were asked whether they considered themselves to be transgender in a separate series of questions from the question about whether they considered themselves to be lesbian, gay, bisexual, or heterosexual (see Appendix 1 for more details).
 
And just as gay men, lesbians, and bisexuals perceive less commonality with transgender people than with each other, transgender adults may appear not to perceive a great deal of commonality with lesbians, gay men, and bisexuals. In particular, issues like same-sex marriage may be viewed as less important by this group, and transgender adults appear to be less involved in the LGBT community than are other sub-groups.
 
Chapter 1, Demographic Portrait and Research Challenges, examines the demographic profile of lesbian, gay, bisexual and transgender adults surveyed by the Pew Research Center and other prominent research organizations. It also includes data on same-sex couples from the U.S. Census Bureau. In addition, this chapter discusses the challenges involved in surveying this population and making estimates about its size and characteristics.
 
Chapter 2, Social Acceptance, looks at societal views of the LGBT population from the perspective of LGBT adults themselves. It also chronicles the ways in which LGBT adults have experienced discrimination in their own lives and looks at the extent to which they believe major institutions in this country are accepting of them.
 
Chapter 3, The Coming Out Experience, chronicles the journey LGBT adults have been on in realizing their sexual orientation or gender identity and sharing that information with family and friends. It also looks at where LGBT adults live, how many of their friends are LGBT and whether they are open about their LGBT identity at work. This chapter includes a brief section on online habits and behaviors.
 
Following the survey chapters is a detailed survey methodology statement. This includes descriptions of the sampling frame, questionnaire development and weighting procedures for the LGBT survey. It also has a demographic profile of the Pew Research LGBT survey respondents with details on specific LGBT groups.
 
*Ulysses* chronicles the appointments and encounters of the itinerant Leopold Bloom in Dublin in the course of an ordinary day, 16 June 1904.[6][7] Ulysses is the Latinised name of Odysseus, the hero of Homer's epic poem the *Odyssey*, and the novel establishes a series of parallels between the poem and the novel, with structural correspondences between the characters and experiences of Bloom and Odysseus, Molly Bloom and Penelope, and Stephen Dedalus and Telemachus, in addition to events and themes of the early 20th-century context of modernism, Dublin, and Ireland's relationship to Britain. The novel is highly allusive and also imitates the styles of different periods of English literature.
 
*Ulysses* is divided into the three books (marked I, II, and III) and 18 episodes. The episodes do not have chapter headings or titles, and are numbered only in Gabler's edition. In the various editions the brea