## Solucionario Holman Transferencia De Calor 8 Edicion 117

 
  
 
**LINK ✫✫✫ [https://shurll.com/2tziAe](https://shurll.com/2tziAe)**

 
 
 
 
 
# Solucionario de transferencia de calor, holman 8 edicion: una guÃ­a prÃ¡ctica para estudiantes de ingenierÃ­a
 
La transferencia de calor es una de las ramas mÃ¡s importantes de la ingenierÃ­a tÃ©rmica, que se ocupa del estudio de los fenÃ³menos relacionados con el flujo de energÃ­a tÃ©rmica entre cuerpos o sistemas con diferentes temperaturas. El conocimiento de la transferencia de calor es fundamental para el diseÃ±o y anÃ¡lisis de sistemas como motores, turbinas, refrigeradores, acondicionadores de aire, intercambiadores de calor, hornos, reactores, etc.
 
El libro *Transferencia de Calor*, escrito por el profesor J.P. Holman, es uno de los textos clÃ¡sicos y mÃ¡s utilizados en la enseÃ±anza de esta materia en las carreras de ingenierÃ­a. La octava ediciÃ³n del libro presenta un tratamiento elemental y actualizado de los fundamentos de la transferencia de calor por conducciÃ³n, convecciÃ³n y radiaciÃ³n, asÃ­ como aplicaciones prÃ¡cticas en diversos campos de la ingenierÃ­a.
 
El solucionario del libro es una herramienta muy Ãºtil para los estudiantes que quieren reforzar su aprendizaje y resolver los ejercicios propuestos al final de cada capÃ­tulo. El solucionario contiene las soluciones detalladas y explicadas de los problemas seleccionados del libro, con el apoyo de grÃ¡ficos, tablas y ecuaciones. El solucionario tambiÃ©n incluye algunos problemas adicionales para ampliar el conocimiento y la habilidad de los estudiantes.
 
En este artÃ­culo se presenta el solucionario del problema 117 del capÃ­tulo 8 del libro, que trata sobre la transferencia de calor por convecciÃ³n forzada en un tubo circular. El problema plantea lo siguiente:

> Se bombea agua a 20Â°C a travÃ©s de un tubo circular liso cuyo diÃ¡metro interno es 25 mm y cuya longitud es 15 m. El tubo estÃ¡ expuesto al aire ambiente a 5Â°C con un coeficiente de pelÃ­cula exterior h = 25 W/mK. Si la velocidad del agua es 2 m/s, determine:
> 
> 
> 1. La temperatura media del agua a la salida del tubo.
> 2. La caÃ­da de presiÃ³n a lo largo del tubo.
> 3. La razÃ³n entre la transferencia total de calor desde el agua y la pÃ©rdida total por fricciÃ³n en el tubo.

La soluciÃ³n del problema se basa en los siguientes pasos:

1. Calcular el nÃºmero de Reynolds (Re) y el factor de fricciÃ³n (f) del flujo de agua en el tubo usando las correlaciones empÃ­ricas adecuadas para flujo turbulento en tubos lisos.
2. Calcular el coeficiente de pelÃ­cula interior (h<sub>i</sub>) y el coeficiente global de transferencia de calor (U) usando las correlaciones empÃ­ricas adecuadas para flujo turbulento en tubos lisos.
3. Calcular la temperatura media del agua a la salida del tubo (T<sub>m,o</sub>) usando la ecuaciÃ³n general para el balance tÃ©rmico en un intercambiador de calor.
4. Calcular la caÃ­da de presiÃ³n a lo largo del tubo (âP) usando la ecuaciÃ³n general para el balance mecÃ¡nico en un flujo compresible.
5. Calcular la razÃ³n entre la transferencia total de calor desde el agua (Q) y la pÃ©rdida total por fricciÃ³n en el tubo (W<sub>f</sub>) usando las definiciones respectivas.

Los resultados obtenidos son los siguientes:

- T<sub>m,o</sub> = 19.32Â°C
- âP = -206.5 kPa
- Q/W<sub 842aa4382a




