## Etiology Of Shock Pdf 20

 
  
 
**Click Here →→→ [https://vercupalo.blogspot.com/?d=2tF324](https://vercupalo.blogspot.com/?d=2tF324)**

 
 
 
 
 
**Objectives:**

Explain the definition of shock.Summarize the evaluation of hemorrhagic shock.Outline management options available for the treatment of hemorrhagic shock.Identify some interprofessional team strategies for improving care and communication to improve patient outcomes in patients with hemorrhagic shock.
Access free multiple choice questions on this topic. 
Though most commonly thought of in the setting of trauma, there are numerous causes of hemorrhagic shock that span many systems. Blunt or penetrating trauma is the most common cause, followed by upper and lower gastrointestinal sources. Obstetrical, vascular, iatrogenic, and even urological sources have all been described. Bleeding may be either external or internal. A substantial amount of blood loss to the point of hemodynamic compromise may occur in the chest, abdomen, or retroperitoneum. The thigh itself can hold up to 1 L to 2 L of blood. Localizing and controlling the source of bleeding is of utmost importance to the treatment of hemorrhagic shock but beyond the scope of this article.[1][2][3][4]
 
The preponderance of hemorrhagic shock cases resulting from trauma is high. During one year, one trauma center reported 62.2% of massive transfusions occur in the setting of trauma. The remaining cases are divided among cardiovascular surgery, critical care, cardiology, obstetrics, and general surgery, with trauma utilizing over 75% of the blood products.
 
As patients age, physiological reserves decrease, the likelihood of anticoagulant use increases, and the number of comorbidities increases. Due to this, elderly patients are less likely to handle the physiological stresses of hemorrhagic shock and may decompensate more quickly.
 
Hemorrhagic shock is due to the depletion of intravascular volume through blood loss to the point of being unable to match the tissues demand for oxygen. As a result, mitochondria are no longer able to sustain aerobic metabolism for the production of oxygen and switch to the less efficient anaerobic metabolism to meet the cellular demand for adenosine triphosphate. In the latter process, pyruvate is produced and converted to lactic acid to regenerate nicotinamide adenine dinucleotide (NAD+) to maintain some degree of cellular respiration in the absence of oxygen.
 
A key factor in the pathophysiology of hemorrhagic shock is the development of trauma-induced coagulopathy. Coagulopathy develops as a combination of several processes. The simultaneous loss of coagulation factors via hemorrhage, hemodilution with resuscitation fluids, and coagulation cascade dysfunction secondary to acidosis and hypothermia have been traditionally thought to be the cause of coagulopathy in trauma. However, this traditional model of trauma-induced coagulopathy may be too limited. Further studies have shown that a degree of coagulopathy begins in 25% to 56% of patients before initiation of the resuscitation. This has led to the recognition of trauma-induced coagulopathy as the sum of two distinct processes: acute coagulopathy of trauma and resuscitation-induced coagulopathy.
 
Trauma-induced coagulopathy is acutely worsened by the presence of acidosis and hypothermia. The activity of coagulation factors, fibrinogen depletion, and platelet quantity are all adversely affected by acidosis. Hypothermia (less than 34 C) compounds coagulopathy by impairing coagulation and is an independent risk factor for death in hemorrhagic shock.
 
Recognizing the degree of blood loss via vital signs and mental status abnormalities is important. The American College of Surgeons Advanced Trauma Life Support (ATLS) hemorrhagic shock classification links the amount of blood loss to expected physiologic responses in a healthy 70 kg patient. As total circulating blood volume accounts for approximately 7% of total body weight, this equals approximately five liters in the average 70 kg male patient.
 
The first step in managing hemorrhagic shock is recognition. Ideally, This should occur before the development of hypotension. Close attention should be paid to physiological responses to low blood volume. Tachycardia, tachypnea, and narrowing pulse pressure may be the initial signs. Cool extremities and delayed capillary refill are signs of peripheral vasoconstriction.[8][9][10][11]
 
Hypotensive resuscitation has been suggested for the hemorrhagic shock patient without head trauma. The aim is to achieve a systolic blood pressure of 90 mmHg in order to maintain tissue perfusion without inducing re-bleeding from recently clotted vessels. Permissive hypotension is a means of restricting fluid administration until hemorrhage is controlled while accepting a short period of suboptimal end-organ perfusion. Studies regarding permissive hypotension have yielded conflicting results and must take into account the type of injury (penetrating versus blunt), the likelihood of intracranial injury, the severity of the injury, as well as proximity to a trauma center, and definitive hemorrhage control.
 
In addition to blood products, products that prevent the breakdown of fibrin in clots, or antifibrinolytics, have been studied for their utility in the treatment of hemorrhagic shock in trauma patients. Several antifibrinolytics have been shown to be safe and effective in elective surgery. The CRASH-2 study was a randomized control trial of tranexamic acid versus placebo in trauma has been shown to decrease overall mortality when given in the first eight hours of injury. Follow-up analysis shows additional benefit to tranexamic acid when given in the first three hours after surgery.
 
While hemorrhage is the most common cause of shock in the trauma patient, other causes of shock are to remain on the differential. Obstructive shock can occur in the setting of tension pneumothorax and cardiac tamponade. These etiologies should be uncovered in the primary survey. In the setting of head or neck trauma, an inadequate sympathetic response, or neurogenic shock, is a type of distributive shock that is caused by a decrease in peripheral vascular resistance. This is suggested by an inappropriately low heart rate in the setting of hypotension. Cardiac contusion and infarctions can result in cardiogenic shock. Finally, other causes should be considered that are not related to trauma or blood loss. In the undifferentiated patient with shock, septic shock and toxic causes are also on the differential.
 
Tachycardia is typically the first abnormal vital sign of hemorrhagic shock. As the body attempts to preserve oxygen delivery to the brain and heart, blood is shunted away from extremities and nonvital organs. This causes cold and modeled extremities with delayed capillary refill. This shunting ultimately leads to worsening acidosis.
 
There are many causes of shock, and it is important to find the cause as soon as possible. Because shock carries high morbidity and mortality, the condition is best managed by an interprofessional team that includes a trauma surgeon, emergency department physician, ICU nurses, general surgeon, internist, and intensivist.
 
Synoptic view of the four types of shock (inner, white field) with the organ systems primarily associated with them (outer corners), sites and mechanisms of manifestation (outside the circle), and pathogenetic and pathophysiologic features (outer and middle sectors of the circle). To maintain clarity, mixed types of shock are not depicted.
 
Typical causes of traumatic hypovolemic shock are large surface burns, chemical burns, and deep skin lesions. The trauma also activates the coagulation cascade and the immune system, potentiating the impairment of the macro- and microcirculation. The inflammatory reaction results in damage to the endothelium, increases capillary leak syndrome, and causes severe coagulopathy (9, 10).
 
In Germany about 280 000 patients are affected by sepsis every year; the incidence is rising every year by about 5.7%, and between 2007 and 2013 the mortality fell from 27.0% to 24.3%. About 35% of these patients suffer from septic shock.
 
Neurogenic shock is a state of imbalance between sympathetic and parasympathetic regulation of cardiac action and vascular smooth muscle. The dominant signs are profound vasodilation with relative hypovolemia while blood volume remains unchanged, at least initially.
 
Pathomechanism of neurogenic shock: Connections in the autonomic system for heart rate and blood pressure regulation. NA, nucleus ambiguus; RVLM, rostral ventrolateral nucleus in the medulla; NTS, nucleus tractus solitarii
 
The main symptoms of cardiogenic shock are agitation, disturbed consciousness, cool extremities, and oliguria. Death in patients in cardiogenic shock is usually caused by hemodynamic instability, multiorgan failure, and systemic inflammation.
 
The pathophysiology of obstructive shock can be classified according to the location of the obstruction in the vascular system in relation to the heart (figure 1). Mechanical intra- or extravascular or luminal factors reduce blood flow in the great vessels or cardiac outflow with a critical drop in cardiac output and global oxygen supply. The result is a state of shock with tissue hypoxia in all organ systems. Common to all these obstructive states is the often rapid, massive drop in cardiac output and blood pressure.
 
Obstructive shock is a condition caused by the obstruction of the great vessels or the heart itself. Although the symptoms resemble those of cardiogenic shock, obstructive shock needs to be clearly distinguished from the latter because it is treated quite differently.
 
Obstructive shock needs immediate causal treatment. Simple measures may suffice, such as changing the position of a patient with caval compression syndrome or adjusting the ventila