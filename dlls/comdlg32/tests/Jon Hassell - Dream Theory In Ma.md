## Jon Hassell - Dream Theory In Ma

 
  
 
**DOWNLOAD ✒ [https://www.google.com/url?q=https%3A%2F%2Fbyltly.com%2F2tIbGE&sa=D&sntz=1&usg=AOvVaw30OU7GDm2MqCBYHufD7urE](https://www.google.com/url?q=https%3A%2F%2Fbyltly.com%2F2tIbGE&sa=D&sntz=1&usg=AOvVaw30OU7GDm2MqCBYHufD7urE)**

 
 
 
 
 
PSF: You're from Memphis originally. Did growing up there in the '50s have any lasting impression on you? The things that are invisible to you are often the things that most surround you. Memphis is /was a cultural crossroads. I had lots of interesting 'cross-cultural experiences', as it would be called today. An African-American man named Henry Barnes was a great friend to my family. I remember his taking me out to a juke joint that he had built himself...real country stuff...made of planks and R.C. Cola signs held together with chicken wire. I remember this incredible musical texture going on but I can't remember what it was exactly. I just remember it was funky and vital. It's only in looking back that things like that come to consciousness as being influential. Also the weather there... it's kind of a sultryplace...humid and hot in the summertime. I suppose that supports my love of the exotic and tropical.
 PSF: How did you start out with learning music?I had taken lessons and learned theory. I did a lot of big band stuff with Glenn Miller-type bands which played on hotel roofs. My father had a cornet that was lying around the house that he had played in the Georgia Tech band. He used to bring it out and play one or two tunes he knew on it. When it came time to study music at school, thatwas what was around the house, so I picked it up. 
 PSF: When you started, who were big influences?

I remember Stan Kenton coming to town with his "Innovations" orchestra... five trombones, five trumpets, five saxophones and a string section with Maynard Fergusonscreaming these high notes.... That was powerful. 


 PSF: How did things change for you when you went away to school? (Eastman School of Music, University of Rochester New York)

That was the first time that I had contact with people who were in the realm of entertainment, in the upper strata of the musical world, if you will. I remember one the first things that I saw there was a live TV broadcast of Lena Horne with Mitch Miller conducting the Rochester Symphony- that made my jaw drop. That didn't happen in Memphis. I hung out with a radical group of student composers who were into Webern, Schoenberg, etc. We considered ourselves apart from the general conservative feel of the Eastman school, which was very "Americana" in feeling.


 PSF: After Eastman school, what were you doing?

 I got my Master's degree there and then I went into the army band in Washington D.C. to avoid getting drafted. After that, I did all my work towards my Ph.D. in musicology at Catholic University but never did the thesis. 

I was doing some tape pieces and reading this serial music journal called Die Reihe from Germany, which had articles by Stockhausen and other so-called post-Webern composers. I was really taken by Stockhausen's "Gesang der Juenglinge" ...one of his firstelectronic pieces. It had a lot of 'sampling' of boy's voices which was then usedas a chordal and structural elements. I had to find out what was behind these clusters of notes and all so I had a grant to study in Cologne for two years along with my wife, a pianist I met at Eastman. I had to learn enough German and French to get by, so doors into the European musical world were cracked.

Irmin (Schmidt) and Holger (Czukay), who later formed the band Can, were also in my classes with Stockhausen. 


 PSF: I asked Irmin about the time you were in Germany and if he thought that you might have stayed and worked with Can. He said that he thought you were really into your own individual work and wanted to follow your own path.

The idea of starting a rock band wouldn't have occurred to me at that point. I was on another track. 


 PSF: What happened when you came back to the States?

I had a fellowship at the Center for Creative and Performing Arts at SUNY Buffalo. This was a Center funded by the Rockefeller Foundation for musicians who were composer/performers. During that time, I met Terry Riley. DavidBehrman was there as one of the school's Fellows, producing for Columbia Records "Music in Our Time " series. Terry Riley's *In C* was done with musicians at the Center so David brought Terry up for a few months while the musicians there rehearsed and recorded it there. My wife also appeared on the recording, playing the pulse piano. It was really a revolutionary idea. This American experience being around Terry and later LaMonte Young was a great antidote to the European experience with Stockhausen...coming into contact with people who were concerned with feeling good via music- not just some intellectual exercise. It was more holistic. It spoke to the whole body. I'm talking about what they would later refer to as 'minimalism.'


 PSF: There's a similarity in your work and Terry's with regard to Indian music.

 Before my ear-opening study with Pran Nath, I wasn't aware of the ornamental delicacies of raga, which I have since tried to assimilate into my vocabulary. The line of melody is precisely drawn against the invisible grid of the tambura. It was amazing to discover such a subtle musical form. There were all these new ways to go between C and D or C and C sharp. I remember listening to Pran Nath do concerts where I'd look up and realize that 20 minutes had gone by and he hadn't gotten past the first two or three notes of the raga. 


 PSF: It does seem like your trumpet playing is very similar to the Indian singing style.

 Just about everything I have, I owe to Pran Nath. For the first few months with him, I learned by singing. A phrase would be sung to you, you'd sing it back and if it was correct, you'd move on to something more complex. If not, you work on it again or do something simpler. It was aural/oral transmission. 

Then I started to try to do that on trumpet. I had to completely forget everything that I'd ever been taught I'm still trying to forget it. It was a matter of trying to make the mouthpiece sound like a voice merged with a conch shell.

 I worked on the 'alap', the slow introductory section, of one raga for the first two years. The whole technique evolved out my having to draw, or figure out a way to draw, the kind of curves he was drawing with his voice.


 PSF: What kind of work were you doing after meeting Terry?

 After the whole *In C* epic, LaMonte Young came up to the school and Terry introduced me to him. Terry actually left New York not long after that and I started playing with LaMonte's group (Theatre of Eternal Music)- this was the late '60s/early '70s. This was another major formative thing for me. Once again, there was this idea of using music as kind of a spiritual enrichment. That went along with my studies with Pran Nath because raga is a kind of a musical yoga or discipline. LaMonte absorbed that Eastern feeling in which everything was slowed down enough so that you could actually experience yourself in relation to what was going on. A flock of notes isn't being thrown at you to process intellectually but instead you're there in a midst of this sound world in which--as you sit longer and longer--you perceive more details and start to listen vertically to the constantly changing structures created by overtones in flux. 


 PSF: It's not music to get stoned with but it's really that you're emulating those effects, right?

 One begets the other. Heightened perception asks for a certain way of listening and that certain way of listening asks for a certain perception. Which comes first, who knows?


PSF: What were you doing after working with LaMonte?

I invented a television lightscreen and was partners in a corporation to exploit it. This sidetracked me from music for a couple of years. About this time my marriage ended and I entered into a new relationship with a girl who was an African-American/Native-American mixture... A bit of Fourth World on a personal level. During this time I started listening to Miles in his electric, wah-wah pedal period and I began practicing again.

 That was one of the things that forced me to focus on the dichotomy between the abstract, white-on-white, pure world of, let's say, classical 'mimimalism' and the world of shadows that happens when the lights get turned down low and it gets sexy and you start to think, 'What kind of music do I want to listen to?' I saw that this dichotomy was a Western thing. As I looked at other musics, I saw that the sensuous part was not left on the cutting room floor -- it was music for above and below the waist simultaneously. This became my sine qua non for music--my own and others.

 I had done an electronic piece called "Solid State" (1969) while I was at the Center in Buffalo which was presented as a 'sound sculpture' in a museum or gallery setting. It was a big solid piece of sound that was gradually eroded into rhythmic shapes by sequential filtering. This was partly a response to the mega sound-surround happening in the club experience of the time. It was also a response to the overtone world that I was experiencing in LaMonte's Theater of Eternal Music performances...all the delicate, filigree stuff that was happening in the overtone spectrum. "Solid State" brought that down into the range of the fundamentals of those overtones. 

Later, I did a version of that piece at the Heiner Friedrich Gallery in New York in where I had Walter De Maria, the earth artist playing dumbek, and I was playing trumpet against the rhythmically-shifting sound mass. 

In retrospect, this can be seen as a transition away from the path of avant-garde 'minimalism'--Glass, Reich, etc. and toward something that began as a painting-in of the essential raga elements--drone back