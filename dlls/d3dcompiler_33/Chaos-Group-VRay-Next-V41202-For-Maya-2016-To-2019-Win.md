## Chaos Group V-Ray Next v4.12.02 for Maya 2016 to 2019 Win

 
  
 
**DOWNLOAD >>> [https://ssurll.com/2tHybc](https://ssurll.com/2tHybc)**

 
 
 
 
 
By Dick Downey  
Posted April 8, 2019   
  
 **Santa Anita Derby**   
  
Game Winner and Instagrand get credit for running "12's". In other words, each of them was within one second of averaging 12 seconds per furlong during at least three of the five calls of the race. See Section 7 below for more information.  
  
However, none of the top three finishers ran the final 3/8 mile in 38 seconds or less. The fact that Game Winner was unable to do so is a reason he didn't win this race.  
  
Roadster was awarded a 98 Beyer Speed Figure from a time of 1:51.28. As Randy Moss observed on NBCSN before the race, times on dirt had been slow all day at the track due to a deep surface. A review of the charts on this card confirms that.  
  
Roadster polished off the field in a professional manner after sitting behind a moderate pace. Jockey Smith didn't use the whip/crop much, and Roadster was the best horse after a decent trip from off the pace that Smith mapped out. Looks like that throat surgery really did work.  
  
Joel Rosario went to work with his whip/crop on Game Winner before they left the far turn. After Game Winner put away the pace-setting Instagrand inside the furlong marker, he couldn't finish strongly enough to win.  
  
Instagrand maintained his lead into the stretch after being under heavy early pressure from Nolo Contesto. However, once he shook that one off, he was unable to take it on to the wire. He may have distance limitations, but he ran a good race. If he runs in the Pat Day Mile, it would be his third race off the layoff.  
  
All three horses ran a fairly straight line down the stretch.  
  
Roadster didn't gallop out very far. Instagrand and Game Winner were in front during the gallop-out.  
  
The Santa Anita Derby became the 13th Kentucky Derby points race this year to be won by a length or less. I prefer a horse than wins by more than that, but we'll see how the next three weeks and five days go.  
  
 **Roadster**   
  
Fractions  
  
1/4 mile -- :23.74 (fifth)  
1/2 mile -- :48.24 (fifth)  
3/4 mile -- 1:13.15 (third)  
1 mile -- 1:38.54 (third)  
1 1/8 mile -- 1:51.28 (first)  
  
Internal Fractions  
first 1/4 mile -- :23.74  
second 1/4 mile -- :24.50  
third 1/4 mile -- :24.91  
fourth 1/4 mile -- :25.39  
final 1/8 mile -- :12.74  
final 3/8 mile -- :38.13  
  
 **Game Winner**   
  
Fractions  
1/4 mile -- :23.74 (fourth)  
1/2 mile -- :48.04 (fourth)  
3/4 mile -- 1:12.30 (third)  
1 mile -- 1:38.44 (second)  
1 1/8 mile -- 1:51.38 (second)  
  
Internal Fractions  
first 1/4 mile -- :23.74  
second 1/4 mile -- :24.30  
third 1/4 mile -- :24.26  
fourth 1/4 mile -- :26.14  
final 1/8 mile -- :12.94  
final 3/8 mile -- :39.08  
  
 **Instagrand**   
  
Fractions  
1/4 mile -- :23.34 (first)  
1/2 mile -- :47.84 (first)  
3/4 mile -- 1:12.20 (first)  
1 mile -- 1:38.34 (first)  
1 1/8 mile -- 1:51.73 (third)  
  
Internal Fractions  
first 1/4 mile -- :23.34  
second 1/4 mile -- :24.50  
third 1/4 mile -- :24.36  
fourth 1/4 mile -- :26.14  
final 1/8 mile -- :13.39  
final 3/8 mile -- :39.53  

 **Toyota Blue Grass Stakes**   
  
Vekoma, Win Win Win, Signalman and Somelikeithotbrown all get credit for running "12's".  
  
None of them finished the final 3/8 mile in 38 seconds or less.  
  
Vekoma was awarded a 94 Beyer Speed Figure with a time of 1:50.93.  
  
In the second race, Honest Mischief, a quality Juddmonte 3-year-old maiden, was timed in 1:22.43 for seven furlongs, winning by eight lengths. In race 6, the seven-furlong Commonwealth Stakes, the improving Bobby's Wicked One was timed in 1:22.80 beating a quality field of older horses. The Madison for fillies and mares four and up was race 8, and it was tmed in 1:23.49. Compared to early in the day, maybe the track had slowed down a bit by the Blue Grass, which was race 10.  
  
I took a stand against Vekoma, reasoning that, breaking from post 2, he wouldn't be able to handle being stuck down on the inside. My reasoning fell apart after Javier Castellano hustled him away from the gate and got him to the outside of pacesetter Somelikeithotbrown, who broke from post 1. Instead of being stuck down on the rail and/or between/behind horses, Vekoma got just the kind of trip he needed -- a trip in the clear.  
  
After he entered the stretch, Vekoma didn't promptly change leads. His paddling left foreleg swung around prominently, he drifted out, and he lost momentum. Still, nothing challenged him. He finally switched leads inside the eighth pole and took off again to win by 3 1/2 lengths. Vekoma may very well win the Kentucky Derby, but he'll need to switch leads at the right time.  
  
Castellano said after the race he's crazy about this horse, that after he worked him he had confidence Vekoma would win the Blue Grass with his speed and multiple gears.  
  
Win Win Win, ridden by Irad Ortiz, Jr., was steadied briefly just past the mid-point of the far turn, but it didn't cost him the win. I agree with the NBC commentators who said he would have been second more easily but for being steadied. I would add that, to his credit, Win Win Win regained his momentum fairly quickly. As he approached the wire, Win Win Win came in a bit.  
  
Signalman saved all the ground and just could not get the job done. He was given every chance by Brian Hernandez, Jr. Signalman, a large horse, is kind of a plodder -- but he does keep running.  
  
Once again, Signalman won the gallop-out with his ears pricked while passing Vekoma. Into the first turn, Win Win Win and Somelikeithotbrown passed Vekoma, too, but Vekoma galloped out well.  
  
Somelikeithotbrown handled the dirt at Keeneland, but he drifted out in the stretch. He impresses me as a pretty good one-turn horse. If he makes the Kentucky Derby, I expect him to be part of the early pace.  
  
 **Vekoma**   
  
Fractions  
1/4 mile -- :23.46 (second)  
1/2 mile -- :47.05 (second)  
3/4 mile -- 1:11.57 (second)  
1 mile -- 1:37.48 (first)  
1 1/8 mile -- 1:50.93 (first)  
  
Internal Fractions  
first 1/4 mile -- :23.46  
second 1/4 mile -- :23.59  
third 1/4 mile -- :24.52  
fourth 1/4 mile -- :25.91  
final 1/8 mile -- :13.45  
final 3/8 mile -- :39.36  
  
 **Win Win Win**   
  
Fractions  
1/4 mile -- :24.91 (thirteenth)  
1/2 mile -- :48.48 (eleventh)  
3/4 mile -- 1:12.90 (eighth)  
1 mile -- 1:39.08 (fifth)  
1 1/8 mile -- 1:51.63 (second)  
  
Internal Fractions  
first 1/4 mile -- :24.91  
second 1/4 mile -- :23.57  
third 1/4 mile -- :24.42  
fourth 1/4 mile -- :26.18  
final 1/8 mile -- :12.55  
final 3/8 mile -- :38.73  
  
 **Signalman**   
  
Fractions  
1/4 mile -- :23.66 (fourth)  
1/2 mile -- :47.38 (fifth)  
3/4 mile -- 1:11.85 (third)  
1 mile -- 1:38.38 (third)  
1 1/8 mile -- 1:51.64 (third)  
  
Internal Fractions  
first 1/4 mile -- :23.66  
second 1/4 mile -- :23.72  
third 1/4 mile -- :24.47  
fourth 1/4 mile -- :26.53  
final 1/8 mile -- :13.26  
final 3/8 mile -- :39.79  
  
 **Somelikeithotbrown**   
  
Fractions  
1/4 mile -- :23.26 (first)  
1/2 mile -- :47.03 (first)  
3/4 mile -- 1:11.55 (first)  
1 mile -- 1:38.18 (second)  
1 1/8 mile -- 1:51.78 (fourth)  
  
Internal Fractions  
first 1/4 mile -- :23.26  
second 1/4 mile -- :23.77  
third 1/4 mile -- :24.52  
fourth 1/4 mile -- :26.63  
final 1/8 mile -- :13.60  
final 3/8 mile -- :40.23  

 **Wood Memorial**   
  
Tacitus and Tax get credit for running "12's".  
  
Only Haikal ran the final 3/8 mile in 38 seconds or less, and he should have after running slowly the first six furlongs.  
  
Tacitus was awarded a 97 Beyer Speed Figure after being timed in 1:51.23.  
  
Joevia broke from post 11, veeried left and sawed off almost all of the inside of the field. It was nearly a disaster. Tacitus, who broke from post 2, was bumped soundly and was checked. Soon after that, I'm not quite sure what happened on the first turn, when Overdeliver was squeezed between Tacitus and Not That Brady, but this was another incident that would bother a lesser horse.  
  
All that said, my point is that Tacitus impressed me favorably because rough stuff doesn't bother him. it's a good trait to possess when you think about all the bumping and crazy things that can happen in the Kentucky Derby. Being a larger horse, Tacitus can dish it out, and he can take it. As Mike "Cheap Speed" Pearson used to say about Smarty Jones, he don't care.  
  
I was wrong about Tax. The top runners from the Withers Stakes had accomplished nothing until Tax came along in this race, and he was very game. In deep stretch, he was pushed over toward the rail a bit by Tacitus, but watching the head-on, I can't say it changed the outcome of the race.  
  
When I wrote that Joevia sawed off almost all of the inside of the field, it's because Tax, who broke from post 1, was spared. He was just beyond the chaos Joevia caused. In my mind, this makes the win by Tacitus even more impressive.  
  
Last week, trainer McLaughlin said Haikal wouldn't be 14 lengths behind this time, but after a half-mile he was 14 1/2 lengths off the pace. He wasn't totally caught up in the melee caused by Joevia, but he was away from the gate a step slow and wound up behind that bunch of bumped horses. To Haikal's credit, on the backstretch he responded when asked, and he made an extended run to get up for third, beaten four lengths. All in all, I guess it was a pretty good effort for his first two-turn race, and he makes you think of a horse that could fill out the bottom of some exotic tickets in the Derby.  
  
Tax and Tacitus won the gallop-out together.  
  
 **Tacitus**   
  
Fractions  
1/4 mile -- :24.82 (fourth)  
1/2 mile -- :48.